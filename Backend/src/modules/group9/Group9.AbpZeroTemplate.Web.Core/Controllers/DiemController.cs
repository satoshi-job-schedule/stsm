﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.Diem;
using Group9.AbpZeroTemplate.Web.Core.Services.Diem.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    class DiemController : AbpController
    {
        private readonly IDiemAppService DiemAppService;

        public DiemController(IDiemAppService DiemAppService)
        {
            this.DiemAppService = DiemAppService;
        }


        [HttpPost]
        public IDictionary<string, object> Diem_Insert([FromBody] DiemDto input)
        {
            return DiemAppService.Diem_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> Diem_Delete(int id)
        {
            return DiemAppService.Diem_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> Diem_Update([FromBody] DiemDto input)
        {
            return DiemAppService.Diem_Update(input);
        }
        [HttpPost]
        public List<DiemDto> Diem_Search([FromBody] DiemDto input)
        {
            return DiemAppService.Diem_Search(input);
        }
        [HttpPost]
        public DiemDto PhucKhao_Group9ById(int id)
        {
            return DiemAppService.Diem_ById(id);
        }
        [HttpPost]

        public List<DiemDto> PhucKhao_Group9SearchAll()
        {
            return DiemAppService.Diem_SearchAll();
        }
    }
}
