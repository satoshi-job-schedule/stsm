﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.MonHoc;
using Group9.AbpZeroTemplate.Web.Core.Services.MonHoc.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    class MonHocController : AbpController
    {
        private readonly IMonHocAppService MonHocAppService;

        public MonHocController(IMonHocAppService MonHocAppService)
        {
            this.MonHocAppService = MonHocAppService;
        }


        [HttpPost]
        public IDictionary<string, object> MonHoc_Insert([FromBody] MonHocDto input)
        {
            return MonHocAppService.MonHoc_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> MonHoc_Delete(int id)
        {
            return MonHocAppService.MonHoc_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> MonHoc_Update([FromBody] MonHocDto input)
        {
            return MonHocAppService.MonHoc_Update(input);
        }
        [HttpPost]
        public List<MonHocDto> MonHoc_Search([FromBody] MonHocDto input)
        {
            return MonHocAppService.MonHoc_Search(input);
        }
        [HttpPost]
        public MonHocDto PhucKhao_Group9ById(int id)
        {
            return MonHocAppService.MonHoc_ById(id);
        }
        [HttpPost]

        public List<MonHocDto> PhucKhao_Group9SearchAll()
        {
            return MonHocAppService.MonHoc_SearchAll();
        }
    }
}
