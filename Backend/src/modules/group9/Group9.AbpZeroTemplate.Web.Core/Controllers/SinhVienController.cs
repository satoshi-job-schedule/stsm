﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.SinhVien;
using Group9.AbpZeroTemplate.Web.Core.Services.SinhVien.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SinhVienController : AbpController
    {
        private readonly ISinhVienAppService sinhVienAppService;

        public SinhVienController(ISinhVienAppService SinhVienAppService)
        {
            this.sinhVienAppService = SinhVienAppService;
        }


        [HttpPost]
        public IDictionary<string, object> SinhVien_Insert([FromBody] SinhVienDto input)
        {
            return sinhVienAppService.SinhVien_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> SinhVien_Delete(int id)
        {
            return sinhVienAppService.SinhVien_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> SinhVien_Update([FromBody] SinhVienDto input)
        {
            return sinhVienAppService.SinhVien_Update(input);
        }
        [HttpPost]
        public List<SinhVienDto> SinhVien_Search([FromBody] SinhVienDto input)
        {
            return sinhVienAppService.SinhVien_Search(input);
        }
        [HttpPost]
        public SinhVienDto PhucKhao_Group9ById(int id)
        {
            return sinhVienAppService.SinhVien_ById(id);
        }
        [HttpPost]

        public List<SinhVienDto> PhucKhao_Group9SearchAll()
        {
            return sinhVienAppService.SinhVien_SearchAll();
        }
    }
}