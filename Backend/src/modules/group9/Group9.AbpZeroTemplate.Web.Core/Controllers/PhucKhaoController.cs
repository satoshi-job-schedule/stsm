﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.PhucKhao;
using Group9.AbpZeroTemplate.Web.Core.Services.PhucKhao.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    public class PhucKhaoController : AbpController
    {
        private readonly IPhucKhaoAppService PhucKhaoAppService;

        public PhucKhaoController(IPhucKhaoAppService PhucKhaoAppService)
        {
            this.PhucKhaoAppService = PhucKhaoAppService;
        }


        [HttpPost]
        public IDictionary<string, object> PhucKhao_Insert([FromBody] PhucKhaoDto input)
        {
            return PhucKhaoAppService.PhucKhao_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> PhucKhao_Delete(int id)
        {
            return PhucKhaoAppService.PhucKhao_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> PhucKhao_Update([FromBody] PhucKhaoDto input)
        {
            return PhucKhaoAppService.PhucKhao_Update(input);
        }
        [HttpPost]
        public List<PhucKhaoDto> PhucKhao_Search([FromBody] PhucKhaoDto input)
        {
            return PhucKhaoAppService.PhucKhao_Search(input);
        }
        [HttpPost]
        public PhucKhaoDto PhucKhao_Group9ById(int id)
        {
            return PhucKhaoAppService.PhucKhao_ById(id);
        }
        [HttpPost]

        public List<PhucKhaoDto> PhucKhao_Group9SearchAll()
        {
            return PhucKhaoAppService.PhucKhao_SearchAll();
        }
    }
}