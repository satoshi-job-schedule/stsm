﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.LopHoc;
using Group9.AbpZeroTemplate.Web.Core.Services.LopHoc.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    class LopHocController : AbpController
    {
        private readonly ILopHocAppService LopHocAppService;

        public LopHocController(ILopHocAppService LopHocAppService)
        {
            this.LopHocAppService = LopHocAppService;
        }


        [HttpPost]
        public IDictionary<string, object> LopHoc_Insert([FromBody] LopHocDto input)
        {
            return LopHocAppService.LopHoc_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> LopHoc_Delete(int id)
        {
            return LopHocAppService.LopHoc_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> LopHoc_Update([FromBody] LopHocDto input)
        {
            return LopHocAppService.LopHoc_Update(input);
        }
        [HttpPost]
        public List<LopHocDto> LopHoc_Search([FromBody] LopHocDto input)
        {
            return LopHocAppService.LopHoc_Search(input);
        }
        [HttpPost]
        public LopHocDto PhucKhao_Group9ById(int id)
        {
            return LopHocAppService.LopHoc_ById(id);
        }
        [HttpPost]

        public List<LopHocDto> PhucKhao_Group9SearchAll()
        {
            return LopHocAppService.LopHoc_SearchAll();
        }
    }
}
