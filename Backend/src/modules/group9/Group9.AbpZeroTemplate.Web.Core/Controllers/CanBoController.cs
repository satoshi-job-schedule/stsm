﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.CanBo;
using Group9.AbpZeroTemplate.Web.Core.Services.CanBo.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    class CanBoController : AbpController
    {
        private readonly ICanBoAppService CanBoAppService;

        public CanBoController(ICanBoAppService CanBoAppService)
        {
            this.CanBoAppService = CanBoAppService;
        }


        [HttpPost]
        public IDictionary<string, object> CanBo_Insert([FromBody] CanBoDto input)
        {
            return CanBoAppService.CanBo_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> CanBo_Delete(int id)
        {
            return CanBoAppService.CanBo_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> CanBo_Update([FromBody] CanBoDto input)
        {
            return CanBoAppService.CanBo_Update(input);
        }
        [HttpPost]
        public List<CanBoDto> CanBo_Search([FromBody] CanBoDto input)
        {
            return CanBoAppService.CanBo_Search(input);
        }
        [HttpPost]
        public CanBoDto PhucKhao_Group9ById(int id)
        {
            return CanBoAppService.CanBo_ById(id);
        }
        [HttpPost]

        public List<CanBoDto> PhucKhao_Group9SearchAll()
        {
            return CanBoAppService.CanBo_SearchAll();
        }
    }
}
