﻿using Abp.AspNetCore.Mvc.Controllers;
using Group9.AbpZeroTemplate.Web.Core.Services.LichThi;
using Group9.AbpZeroTemplate.Web.Core.Services.LichThi.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Application.Controllers
{
    [Route("api/[controller]/[action]")]
    class LichThiController : AbpController
    {
        private readonly ILichThiAppService LichThiAppService;

        public LichThiController(ILichThiAppService LichThiAppService)
        {
            this.LichThiAppService = LichThiAppService;
        }


        [HttpPost]
        public IDictionary<string, object> LichThi_Insert([FromBody] LichThiDto input)
        {
            return LichThiAppService.LichThi_Insert(input);
        }
        [HttpPost]
        public IDictionary<string, object> LichThi_Delete(int id)
        {
            return LichThiAppService.LichThi_Delete(id);
        }
        [HttpPost]
        public IDictionary<string, object> LichThi_Update([FromBody] LichThiDto input)
        {
            return LichThiAppService.LichThi_Update(input);
        }
        [HttpPost]
        public List<LichThiDto> LichThi_Search([FromBody] LichThiDto input)
        {
            return LichThiAppService.LichThi_Search(input);
        }
        [HttpPost]
        public LichThiDto PhucKhao_Group9ById(int id)
        {
            return LichThiAppService.LichThi_ById(id);
        }
        [HttpPost]

        public List<LichThiDto> PhucKhao_Group9SearchAll()
        {
            return LichThiAppService.LichThi_SearchAll();
        }
    }
}
