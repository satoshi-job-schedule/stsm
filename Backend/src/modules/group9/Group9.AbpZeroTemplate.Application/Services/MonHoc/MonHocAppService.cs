﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.MonHoc.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.MonHoc
{
    public interface IMonHocAppService : IApplicationService
    {
        IDictionary<string, object> MonHoc_Update(MonHocDto input);
        MonHocDto MonHoc_ById(int id);
        IDictionary<string, object> MonHoc_Insert(MonHocDto input);
        IDictionary<string, object> MonHoc_Delete(int id);
        List<MonHocDto> MonHoc_Search(MonHocDto input);
        List<MonHocDto> MonHoc_SearchAll();

    }
    public class Group9HangAppService : BaseService, IMonHocAppService
    {
        public Group9HangAppService()
        {

        }

        public MonHocDto MonHoc_ById(int id)
        {
            return procedureHelper.GetData<MonHocDto>("MonHoc_ById", new
            {
                MonHoc_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> MonHoc_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("MonHoc_Delete", new
            {
                MonHoc_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> MonHoc_Insert(MonHocDto input)
        {
            return procedureHelper.GetData<dynamic>("MonHoc_Insert", input).FirstOrDefault();
        }

        public List<MonHocDto> MonHoc_Search(MonHocDto input)
        {
            return procedureHelper.GetData<MonHocDto>("MonHoc_Search", input);
        }

        public List<MonHocDto> MonHoc_SearchAll()
        {
            return procedureHelper.GetData<MonHocDto>("MonHoc_SearchAll", new { });
        }

        public IDictionary<string, object> MonHoc_Update(MonHocDto input)
        {
            return procedureHelper.GetData<dynamic>("MonHoc_Update", input).FirstOrDefault();
        }
    }
}
