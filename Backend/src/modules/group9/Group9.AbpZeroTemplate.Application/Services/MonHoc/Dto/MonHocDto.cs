﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.MonHoc.Dto
{
	public class MonHocDto
    {
		public int MONHOC_Ma { get; set; }
		public string MONHOC_MaMon { get; set; }
		public string MONHOC_TenMon { get; set; }
		public string MONHOC_Khoa { get; set; }
		public string MONHOC_SoTC { get; set; }
		public string MONHOC_CMND { get; set; }
		public float MONHOC_TCLyThuyet { get; set; }
		public float MONHOC_TCThucHanh { get; set; }
		public double MONHOC_HeSoQT { get; set; }
		public double MONHOC_HeSoGK { get; set; }
		public double MONHOC_HeSoTH { get; set; }
		public double MONHOC_HeSoCK { get; set; }
		public string MONHOC_NguoiTao { get; set; }
		public DateTime MONHOC_NgayTao { get; set; }
		public string MONHOC_TrangThai { get; set; }
	}
}
