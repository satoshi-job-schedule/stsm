﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.PhucKhao.Dto
{
    public class PhucKhaoDto
    {
		public int PHUCKHAO_Ma { get; set; }
		public string PHUCKHAO_MaPhucKhao { get; set; }
		public string PHUCKHAO_MSSV { get; set; }
		public string PHUCKHAO_HoTen { get; set; }
		public string PHUCKHAO_Khoa { get; set; }
		public string PHUCKHAO_MaLop { get; set; }
		public string PHUCKHAO_CotDiemPK { get; set; }
		public double PHUCKHAO_ThoiGianPK { get; set; }
		public double PHUCKHAO_DiemCu { get; set; }
		public double PHUCKHAO_DiemMoi { get; set; }
		public string PHUCKHAO_NguoiTao { get; set; }
		public DateTime PHUCKHAO_NgayTao { get; set; }
		public string PHUCKHAO_TrangThai { get; set; }
	}
}
