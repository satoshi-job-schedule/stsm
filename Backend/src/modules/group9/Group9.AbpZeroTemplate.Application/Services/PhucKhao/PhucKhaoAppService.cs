﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.PhucKhao.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.PhucKhao
{
    public interface IPhucKhaoAppService : IApplicationService
    {
        IDictionary<string, object> PhucKhao_Update(PhucKhaoDto input);
        PhucKhaoDto PhucKhao_ById(int id);
        IDictionary<string, object> PhucKhao_Insert(PhucKhaoDto input);
        IDictionary<string, object> PhucKhao_Delete(int id);
        List<PhucKhaoDto> PhucKhao_Search(PhucKhaoDto input);
        List<PhucKhaoDto> PhucKhao_SearchAll();

    }
    public class Group9HangAppService : BaseService, IPhucKhaoAppService
    {
        public Group9HangAppService()
        {

        }

        public PhucKhaoDto PhucKhao_ById(int id)
        {
            return procedureHelper.GetData<PhucKhaoDto>("PhucKhao_ById", new
            {
                PhucKhao_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> PhucKhao_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("PhucKhao_Delete", new
            {
                PhucKhao_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> PhucKhao_Insert(PhucKhaoDto input)
        {
            return procedureHelper.GetData<dynamic>("PhucKhao_Insert", input).FirstOrDefault();
        }

        public List<PhucKhaoDto> PhucKhao_Search(PhucKhaoDto input)
        {
            return procedureHelper.GetData<PhucKhaoDto>("PhucKhao_Search", input);
        }

        public List<PhucKhaoDto> PhucKhao_SearchAll()
        {
            return procedureHelper.GetData<PhucKhaoDto>("PhucKhao_SearchAll", new { });
        }

        public IDictionary<string, object> PhucKhao_Update(PhucKhaoDto input)
        {
            return procedureHelper.GetData<dynamic>("PhucKhao_Update", input).FirstOrDefault();
        }
    }
}
