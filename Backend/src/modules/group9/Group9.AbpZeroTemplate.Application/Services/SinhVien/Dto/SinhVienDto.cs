﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.SinhVien.Dto
{
    public class SinhVienDto
    {
		public int SINHVIEN_Ma { get; set; }
		public string SINHVIEN_MSSV { get; set; }
		public string SINHVIEN_HoTen { get; set; }
		public string SINHVIEN_GioiTinh { get; set; }
		public DateTime SINHVIEN_NgaySinh { get; set; }
		public string SINHVIEN_CMND { get; set; }
		public string SINHVIEN_DiaChi { get; set; }
		public string SINHVIEN_KhoaHoc { get; set; }
		public string SINHVIEN_SoDienThoai { get; set; }
		public string SINHVIEN_Khoa { get; set; }
		public string SINHVIEN_Nganh { get; set; }
		public string SINHVIEN_Lop { get; set; }
		public string SINHVIEN_Email { get; set; }
		public string SINHVIEN_TinhTrang { get; set; }
		public string SINHVIEN_NguoiTao { get; set; }
		public DateTime SINHVIEN_NgayTao { get; set; }
		public string SINHVIEN_TrangThai { get; set; }
	}
}
