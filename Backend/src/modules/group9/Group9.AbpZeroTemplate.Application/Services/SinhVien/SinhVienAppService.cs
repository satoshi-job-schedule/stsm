﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.SinhVien.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.SinhVien
{

    public interface ISinhVienAppService : IApplicationService
    {
        IDictionary<string, object> SinhVien_Update(SinhVienDto input);
        SinhVienDto SinhVien_ById(int id);
        IDictionary<string, object> SinhVien_Insert(SinhVienDto input);
        IDictionary<string, object> SinhVien_Delete(int id);
        List<SinhVienDto> SinhVien_Search(SinhVienDto input);
        List<SinhVienDto> SinhVien_SearchAll();

    }
    public class Group9HangAppService : BaseService, ISinhVienAppService
    {
        public Group9HangAppService()
        {

        }

        public SinhVienDto SinhVien_ById(int id)
        {
            return procedureHelper.GetData<SinhVienDto>("SinhVien_ById", new
            {
                SINHVIEN_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> SinhVien_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("SinhVien_Delete", new
            {
                SINHVIEN_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> SinhVien_Insert(SinhVienDto input)
        {
            return procedureHelper.GetData<dynamic>("SinhVien_Insert", input).FirstOrDefault();
        }

        public List<SinhVienDto> SinhVien_Search(SinhVienDto input)
        {
            return procedureHelper.GetData<SinhVienDto>("SinhVien_Search", input);
        }

        public List<SinhVienDto> SinhVien_SearchAll()
        {
            return procedureHelper.GetData<SinhVienDto>("SinhVien_SearchAll", new { });
        }

        public IDictionary<string, object> SinhVien_Update(SinhVienDto input)
        {
            return procedureHelper.GetData<dynamic>("SinhVien_Update", input).FirstOrDefault();
        }
    }
}
