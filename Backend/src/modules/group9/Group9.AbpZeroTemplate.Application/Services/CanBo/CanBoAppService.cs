﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.CanBo.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.CanBo
{
    public interface ICanBoAppService : IApplicationService
    {
        IDictionary<string, object> CanBo_Update(CanBoDto input);
        CanBoDto CanBo_ById(int id);
        IDictionary<string, object> CanBo_Insert(CanBoDto input);
        IDictionary<string, object> CanBo_Delete(int id);
        List<CanBoDto> CanBo_Search(CanBoDto input);
        List<CanBoDto> CanBo_SearchAll();

    }
    public class Group9HangAppService : BaseService, ICanBoAppService
    {
        public Group9HangAppService()
        {

        }

        public CanBoDto CanBo_ById(int id)
        {
            return procedureHelper.GetData<CanBoDto>("CanBo_ById", new
            {
                CanBo_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> CanBo_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("CanBo_Delete", new
            {
                CanBo_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> CanBo_Insert(CanBoDto input)
        {
            return procedureHelper.GetData<dynamic>("CanBo_Insert", input).FirstOrDefault();
        }

        public List<CanBoDto> CanBo_Search(CanBoDto input)
        {
            return procedureHelper.GetData<CanBoDto>("CanBo_Search", input);
        }

        public List<CanBoDto> CanBo_SearchAll()
        {
            return procedureHelper.GetData<CanBoDto>("CanBo_SearchAll", new { });
        }

        public IDictionary<string, object> CanBo_Update(CanBoDto input)
        {
            return procedureHelper.GetData<dynamic>("CanBo_Update", input).FirstOrDefault();
        }
    }
}
