﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.CanBo.Dto
{
	public class CanBoDto
    {
		public int CANBO_Ma { get; set; }
		public string CANBO_MSCB { get; set; }
		public string CANBO_HoTen { get; set; }
		public string CANBO_GioiTinh { get; set; }
		public DateTime CANBO_NgaySinh { get; set; }
		public string CANBO_CMND { get; set; }
		public string CANBO_DiaChi { get; set; }
		public string CANBO_KhoaHoc { get; set; }
		public string CANBO_SoDienThoai { get; set; }
		public string CANBO_Khoa { get; set; }
		public string CANBO_Nganh { get; set; }
		public string CANBO_Lop { get; set; }
		public string CANBO_Email { get; set; }
		public string CANBO_TinhTrang { get; set; }
		public string CANBO_NguoiTao { get; set; }
		public DateTime CANBO_NgayTao { get; set; }
		public string CANBO_TrangThai { get; set; }
	}
}
