﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.LichThi.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.LichThi
{
    public interface ILichThiAppService : IApplicationService
    {
        IDictionary<string, object> LichThi_Update(LichThiDto input);
        LichThiDto LichThi_ById(int id);
        IDictionary<string, object> LichThi_Insert(LichThiDto input);
        IDictionary<string, object> LichThi_Delete(int id);
        List<LichThiDto> LichThi_Search(LichThiDto input);
        List<LichThiDto> LichThi_SearchAll();

    }
    public class Group9HangAppService : BaseService, ILichThiAppService
    {
        public Group9HangAppService()
        {

        }

        public LichThiDto LichThi_ById(int id)
        {
            return procedureHelper.GetData<LichThiDto>("LichThi_ById", new
            {
                LichThi_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> LichThi_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("LichThi_Delete", new
            {
                LichThi_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> LichThi_Insert(LichThiDto input)
        {
            return procedureHelper.GetData<dynamic>("LichThi_Insert", input).FirstOrDefault();
        }

        public List<LichThiDto> LichThi_Search(LichThiDto input)
        {
            return procedureHelper.GetData<LichThiDto>("LichThi_Search", input);
        }

        public List<LichThiDto> LichThi_SearchAll()
        {
            return procedureHelper.GetData<LichThiDto>("LichThi_SearchAll", new { });
        }

        public IDictionary<string, object> LichThi_Update(LichThiDto input)
        {
            return procedureHelper.GetData<dynamic>("LichThi_Update", input).FirstOrDefault();
        }
    }
}
