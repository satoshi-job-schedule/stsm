﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.LichThi.Dto
{
	public class LichThiDto
    {
		public int LICHTHI_Ma { get; set; }
		public string LICHTHI_MaLichThi { get; set; }
		public string LICHTHI_MaLopHoc { get; set; }
		public string LICHTHI_MaKyThi { get; set; }

		public byte LICHTHI_CaThi { get; set; }
		public byte LICHTHI_ThuThi { get; set; }
		public DateTime LICHTHI_NgayThi { get; set; }
		public string LICHTHI_PhongThi { get; set; }
		public string LICHTHI_CanBo1 { get; set; }
		public string LICHTHI_CanBo2 { get; set; }
		public string LICHTHI_NguoiTao { get; set; }
		public DateTime LICHTHI_NgayTao { get; set; }
		public string LICHTHI_TrangThai { get; set; }

	}
}
