﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.Diem.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.Diem
{
    public interface IDiemAppService : IApplicationService
    {
        IDictionary<string, object> Diem_Update(DiemDto input);
        DiemDto Diem_ById(int id);
        IDictionary<string, object> Diem_Insert(DiemDto input);
        IDictionary<string, object> Diem_Delete(int id);
        List<DiemDto> Diem_Search(DiemDto input);
        List<DiemDto> Diem_SearchAll();

    }
    public class Group9HangAppService : BaseService, IDiemAppService
    {
        public Group9HangAppService()
        {

        }

        public DiemDto Diem_ById(int id)
        {
            return procedureHelper.GetData<DiemDto>("Diem_ById", new
            {
                Diem_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> Diem_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("Diem_Delete", new
            {
                Diem_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> Diem_Insert(DiemDto input)
        {
            return procedureHelper.GetData<dynamic>("Diem_Insert", input).FirstOrDefault();
        }

        public List<DiemDto> Diem_Search(DiemDto input)
        {
            return procedureHelper.GetData<DiemDto>("Diem_Search", input);
        }

        public List<DiemDto> Diem_SearchAll()
        {
            return procedureHelper.GetData<DiemDto>("Diem_SearchAll", new { });
        }

        public IDictionary<string, object> Diem_Update(DiemDto input)
        {
            return procedureHelper.GetData<dynamic>("Diem_Update", input).FirstOrDefault();
        }
    }
}
