﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.Diem.Dto
{
	public class DiemDto
    {
		public int DIEM_Ma { get; set; }
		public string DIEM_MaSV { get; set; }
		public string DIEM_MaLopHoc { get; set; }
		public string DIEM_DiemQT { get; set; }
		public string DIEM_DiemGK { get; set; }
		public string DIEM_DiemTH { get; set; }
		public string DIEM_DiemCK { get; set; }
		public string DIEM_DiemTB { get; set; }
		public string DIEM_TinhTrang { get; set; }
		public string DIEM_NguoiTao { get; set; }
		public DateTime DIEM_NgayTao { get; set; }
		public string DIEM_TrangThai { get; set; }
	}
}
