﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.User.Dto
{
    public class UserDto
    {
        public int USER_Ma { get; set; }
        public string USER_Username { get; set; }
        public string USER_Password { get; set; }
    }
}
