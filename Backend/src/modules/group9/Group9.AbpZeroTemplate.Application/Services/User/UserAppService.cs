﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.User.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.User
{
    public interface IUserAppService : IApplicationService
    {
        IDictionary<string, object> User_Update(UserDto input);
        UserDto User_ById(int id);
        IDictionary<string, object> User_Insert(UserDto input);
        IDictionary<string, object> User_Delete(int id);
        List<UserDto> User_Search(UserDto input);
        List<UserDto> User_SearchAll();

    }
    public class Group9HangAppService : BaseService, IUserAppService
    {
        public Group9HangAppService()
        {

        }

        public UserDto User_ById(int id)
        {
            return procedureHelper.GetData<UserDto>("User_ById", new
            {
                User_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> User_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("User_Delete", new
            {
                User_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> User_Insert(UserDto input)
        {
            return procedureHelper.GetData<dynamic>("User_Insert", input).FirstOrDefault();
        }

        public List<UserDto> User_Search(UserDto input)
        {
            return procedureHelper.GetData<UserDto>("User_Search", input);
        }

        public List<UserDto> User_SearchAll()
        {
            return procedureHelper.GetData<UserDto>("User_SearchAll", new { });
        }

        public IDictionary<string, object> User_Update(UserDto input)
        {
            return procedureHelper.GetData<dynamic>("User_Update", input).FirstOrDefault();
        }
    }
}
