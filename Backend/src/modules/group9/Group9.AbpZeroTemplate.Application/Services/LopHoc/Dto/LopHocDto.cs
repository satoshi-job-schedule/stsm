﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.LopHoc.Dto
{
	public class LopHocDto
    {
		public int LOPHOC_Ma { get; set; }
		public string LOPHOC_MaLopHoc { get; set; }
		public string LOPHOC_MaMon { get; set; }
		public string LOPHOC_MaCB { get; set; }
		public string LOPHOC_PhongHoc { get; set; }
		public string LOPHOC_KhoaHoc { get; set; }
		public string LOPHOC_NamHoc { get; set; }
		public string LOPHOC_KyHoc { get; set; }
		public byte LOPHOC_Thu { get; set; }
		public string LOPHOC_Tiet { get; set; }
		public string LOPHOC_SoSinhVien { get; set; }
		public DateTime LOPHOC_NgayBatDau { get; set; }
		public DateTime LOPHOC_NgayKetThuc { get; set; }
		public string LOPHOC_NgonNgu { get; set; }
		public string LOPHOC_NguoiTao { get; set; }
		public DateTime LOPHOC_NgayTao { get; set; }
		public string LOPHOC_TrangThai { get; set; }

	}
}
