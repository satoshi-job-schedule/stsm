﻿using Abp.Application.Services;
using Group9.AbpZeroTemplate.Web.Core.Services.LopHoc.Dto;
using GSoft.AbpZeroTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group9.AbpZeroTemplate.Web.Core.Services.LopHoc
{
    public interface ILopHocAppService : IApplicationService
    {
        IDictionary<string, object> LopHoc_Update(LopHocDto input);
        LopHocDto LopHoc_ById(int id);
        IDictionary<string, object> LopHoc_Insert(LopHocDto input);
        IDictionary<string, object> LopHoc_Delete(int id);
        List<LopHocDto> LopHoc_Search(LopHocDto input);
        List<LopHocDto> LopHoc_SearchAll();

    }
    public class Group9HangAppService : BaseService, ILopHocAppService
    {
        public Group9HangAppService()
        {

        }

        public LopHocDto LopHoc_ById(int id)
        {
            return procedureHelper.GetData<LopHocDto>("LopHoc_ById", new
            {
                LopHoc_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> LopHoc_Delete(int id)
        {
            return procedureHelper.GetData<dynamic>("LopHoc_Delete", new
            {
                LopHoc_Ma = id
            }).FirstOrDefault();
        }

        public IDictionary<string, object> LopHoc_Insert(LopHocDto input)
        {
            return procedureHelper.GetData<dynamic>("LopHoc_Insert", input).FirstOrDefault();
        }

        public List<LopHocDto> LopHoc_Search(LopHocDto input)
        {
            return procedureHelper.GetData<LopHocDto>("LopHoc_Search", input);
        }

        public List<LopHocDto> LopHoc_SearchAll()
        {
            return procedureHelper.GetData<LopHocDto>("LopHoc_SearchAll", new { });
        }

        public IDictionary<string, object> LopHoc_Update(LopHocDto input)
        {
            return procedureHelper.GetData<dynamic>("LopHoc_Update", input).FirstOrDefault();
        }
    }
}
