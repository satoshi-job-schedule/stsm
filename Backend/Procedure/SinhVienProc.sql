﻿CREATE TABLE [SINHVIEN]
(
 [SINHVIEN_Ma]               int NOT NULL IDENTITY(1,1) ,
 [SINHVIEN_MSSV]				nvarchar(50) NULL ,
 [SINHVIEN_HoTen]			nvarchar(50) NULL ,
 [SINHVIEN_GioiTinh]			nvarchar(50) NULL ,
 [SINHVIEN_NgaySinh]			datetime ,
 [SINHVIEN_CMND]				nvarchar(50) NULL ,
 [SINHVIEN_DiaChi]				nvarchar(50) NULL ,
 [SINHVIEN_KhoaHoc]				nvarchar(50) NULL ,
 [SINHVIEN_SoDienThoai]				nvarchar(50) NULL ,
 [SINHVIEN_Khoa]				nvarchar(50) NULL ,
 [SINHVIEN_Nganh]				nvarchar(50) NULL ,
 [SINHVIEN_Lop]				nvarchar(50) NULL ,
 [SINHVIEN_Email]				nvarchar(50) NULL ,
 [SINHVIEN_TinhTrang]				nvarchar(50) NULL ,
 [SINHVIEN_NguoiTao]  nvarchar(50) NULL ,
 [SINHVIEN_NgayTao]   datetime NULL ,
 [SINHVIEN_TrangThai] varchar(1) NULL ,
);
GO
-------------------[dbo].[SINHVIEN_Insert] 6/27/2020----------------
create or alter proc [dbo].[SINHVIEN_Insert]
@SINHVIEN_MSSV varchar(50) NULL,
@SINHVIEN_HoTen varchar(50) null,
@SINHVIEN_GioiTinh nvarchar(50) NULL,
@SINHVIEN_NgaySinh datetime NULL,
@SINHVIEN_CMND nvarchar(50) null,
@SINHVIEN_DiaChi varchar(50) NULL ,
@SINHVIEN_KhoaHoc  nvarchar(50) NULL ,
@SINHVIEN_SoDienThoai   nvarchar(50) NULL ,
@SINHVIEN_Khoa nvarchar(50) null,
@SINHVIEN_Nganh nvarchar(50) null,
@SINHVIEN_Lop nvarchar(50) null,
@SINHVIEN_Email nvarchar(50) null,
@SINHVIEN_TinhTrang	nvarchar(50) NULL ,
@SINHVIEN_NguoiTao  nvarchar(50) NULL ,
@SINHVIEN_NgayTao   datetime NULL ,
@SINHVIEN_TrangThai varchar(1) NULL 
as

if(exists(select * from SINHVIEN where SINHVIEN_MSSV= @SINHVIEN_MSSV))
begin
	select '1' as Result, N'Sinh viên đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[SINHVIEN]
    ( 
 [SINHVIEN_MSSV]				 ,
 [SINHVIEN_HoTen]			 ,
 [SINHVIEN_GioiTinh]			 ,
 [SINHVIEN_NgaySinh]			 ,
 [SINHVIEN_CMND]				 ,
 [SINHVIEN_DiaChi]				 ,
 [SINHVIEN_KhoaHoc]				 ,
 [SINHVIEN_SoDienThoai]				 ,
 [SINHVIEN_Khoa]				 ,
 [SINHVIEN_Nganh]				 ,
 [SINHVIEN_Lop]				 ,
 [SINHVIEN_Email]				 ,
 [SINHVIEN_TinhTrang]				 ,
 [SINHVIEN_NguoiTao]   ,
 [SINHVIEN_NgayTao]    ,
 [SINHVIEN_TrangThai]  )
	VALUES(   
 @SINHVIEN_MSSV				 ,
 @SINHVIEN_HoTen			 ,
 @SINHVIEN_GioiTinh			 ,
 @SINHVIEN_NgaySinh			 ,
 @SINHVIEN_CMND				 ,
 @SINHVIEN_DiaChi				 ,
 @SINHVIEN_KhoaHoc				 ,
 @SINHVIEN_SoDienThoai				 ,
 @SINHVIEN_Khoa				 ,
 @SINHVIEN_Nganh				 ,
 @SINHVIEN_Lop				 ,
 @SINHVIEN_Email				 ,
 'N'				 ,
 @SINHVIEN_NguoiTao   ,
 GETDATE()    ,
 @SINHVIEN_TrangThai )
	declare @Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @Ma as SINHVIEN_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[SINHVIEN_Update] 6/27/2020----------------

create or alter proc [dbo].[SINHVIEN_Update]
    @SINHVIEN_Ma int = NULL,
@SINHVIEN_MSSV varchar(50) NULL,
@SINHVIEN_HoTen varchar(50) null,
@SINHVIEN_GioiTinh nvarchar(50) NULL,
@SINHVIEN_NgaySinh datetime NULL,
@SINHVIEN_CMND nvarchar(50) null,
@SINHVIEN_DiaChi varchar(50) NULL ,
@SINHVIEN_KhoaHoc  nvarchar(50) NULL ,
@SINHVIEN_SoDienThoai   nvarchar(50) NULL ,
@SINHVIEN_Khoa nvarchar(50) null,
@SINHVIEN_Nganh nvarchar(50) null,
@SINHVIEN_Lop nvarchar(50) null,
@SINHVIEN_Email nvarchar(50) null,
@SINHVIEN_TinhTrang	nvarchar(50) NULL ,
@SINHVIEN_NguoiTao  nvarchar(50) NULL ,
@SINHVIEN_NgayTao   datetime NULL ,
@SINHVIEN_TrangThai varchar(1) NULL 

as

if(not exists(select * from SINHVIEN where SINHVIEN_Ma = @SINHVIEN_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[SINHVIEN]
	   SET 
	   [SINHVIEN_MSSV] = @SINHVIEN_MSSV				,
[SINHVIEN_HoTen] = @SINHVIEN_HoTen			,
[SINHVIEN_GioiTinh] = @SINHVIEN_GioiTinh			,
[SINHVIEN_NgaySinh]	= @SINHVIEN_NgaySinh		,
[SINHVIEN_CMND]	 = @SINHVIEN_CMND			,
[SINHVIEN_DiaChi] = @SINHVIEN_DiaChi				,
[SINHVIEN_KhoaHoc] = @SINHVIEN_KhoaHoc				,
[SINHVIEN_SoDienThoai] = @SINHVIEN_SoDienThoai				,
[SINHVIEN_Khoa] = @SINHVIEN_Khoa				,
[SINHVIEN_Nganh] = @SINHVIEN_Nganh				,
[SINHVIEN_Lop] = @SINHVIEN_Lop				,
[SINHVIEN_Email] = @SINHVIEN_Email				,
[SINHVIEN_TinhTrang] =@SINHVIEN_TinhTrang				,
[SINHVIEN_NguoiTao] = @SINHVIEN_NgayTao,
[SINHVIEN_NgayTao] = @SINHVIEN_NguoiTao,
[SINHVIEN_TrangThai] = @SINHVIEN_TrangThai
	WHERE SINHVIEN_Ma = @SINHVIEN_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @SINHVIEN_Ma as SINHVIEN_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[SINHVIEN_ById] 6/27/2020----------------

create or alter proc [dbo].[SINHVIEN_ById]
    @SINHVIEN_Ma int = NULL
as
begin
select *
from SINHVIEN
where SINHVIEN_Ma = @SINHVIEN_Ma and SINHVIEN_TrangThai = 'N'
end
go
-------------------[dbo].[SINHVIEN_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[SINHVIEN_SearchAll]
as
begin
select *
from SINHVIEN
where SINHVIEN_TrangThai = 'N'
end
go
-------------------[dbo].[SINHVIEN_Search] 6/27/2020----------------
create or alter proc [dbo].[SINHVIEN_Search] 
    @SINHVIEN_Ma int = NULL,
@SINHVIEN_MSSV varchar(50) NULL,
@SINHVIEN_HoTen varchar(50) null,
@SINHVIEN_GioiTinh nvarchar(50) NULL,
@SINHVIEN_NgaySinh datetime NULL,
@SINHVIEN_CMND nvarchar(50) null,
@SINHVIEN_DiaChi varchar(50) NULL ,
@SINHVIEN_KhoaHoc  nvarchar(50) NULL ,
@SINHVIEN_SoDienThoai   nvarchar(50) NULL ,
@SINHVIEN_Khoa nvarchar(50) null,
@SINHVIEN_Nganh nvarchar(50) null,
@SINHVIEN_Lop nvarchar(50) null,
@SINHVIEN_Email nvarchar(50) null,
@SINHVIEN_TinhTrang	nvarchar(50) NULL ,
@SINHVIEN_NguoiTao  nvarchar(50) NULL ,
@SINHVIEN_NgayTao   datetime NULL ,
@SINHVIEN_TrangThai varchar(1) NULL 

as
begin
	select * from SINHVIEN
	where (@SINHVIEN_Ma is null or SINHVIEN_Ma = @SINHVIEN_Ma)
	and (@SINHVIEN_MSSV is null or [SINHVIEN_MSSV] = @SINHVIEN_MSSV)
	and (@SINHVIEN_HoTen is null or SINHVIEN_HoTen= @SINHVIEN_HoTen)
	and (@SINHVIEN_GioiTinh is null or [SINHVIEN_GioiTinh] = @SINHVIEN_GioiTinh)
	and (@SINHVIEN_NgaySinh = null or [SINHVIEN_NgaySinh]	= @SINHVIEN_NgaySinh)
	and (@SINHVIEN_CMND is null or [SINHVIEN_CMND]	 = @SINHVIEN_CMND)
	and (@SINHVIEN_Diachi is null or [SINHVIEN_DiaChi] = @SINHVIEN_DiaChi)
	and (@SINHVIEN_KhoaHoc is null or [SINHVIEN_KhoaHoc] = @SINHVIEN_KhoaHoc)
	and (@SINHVIEN_SoDienThoai is null or [SINHVIEN_SoDienThoai] = @SINHVIEN_SoDienThoai)
	and (@SINHVIEN_Khoa is null or [SINHVIEN_Khoa] = @SINHVIEN_Khoa)
	and (@SINHVIEN_Nganh is null or [SINHVIEN_Nganh] = @SINHVIEN_Nganh)
	and (@SINHVIEN_Lop is null or [SINHVIEN_Lop] = @SINHVIEN_Lop)
	and (@SINHVIEN_Email is null or [SINHVIEN_Email] = @SINHVIEN_Email)
	and (@SINHVIEN_TinhTrang is null or [SINHVIEN_TinhTrang] =@SINHVIEN_TinhTrang)
	and (@SINHVIEN_NgayTao is null or [SINHVIEN_NguoiTao] = @SINHVIEN_NgayTao)
	and (@SINHVIEN_NguoiTao is null or [SINHVIEN_NgayTao] = @SINHVIEN_NguoiTao)
	and (@SINHVIEN_TrangThai is null or [SINHVIEN_TrangThai] = @SINHVIEN_TrangThai)
end
go
-------------------[dbo].[SINHVIEN_Delete] 6/27/2020----------------
create or alter proc [dbo].[SINHVIEN_Delete] @SINHVIEN_Ma int = NULL
as
begin transaction
begin try
	update SINHVIEN set SINHVIEN_TrangThai = 'X' where SINHVIEN_Ma = @SINHVIEN_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @SINHVIEN_Ma as SINHVIEN_Ma
end try
begin catch

rollback transaction

end catch
go
