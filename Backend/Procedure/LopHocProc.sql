﻿CREATE TABLE [LOPHOC]
(
 [LOPHOC_Ma]               int NOT NULL IDENTITY(1,1) ,
 [LOPHOC_MaLopHoc]				nvarchar(50) NULL ,
 [LOPHOC_MaMon]			nvarchar(50) NULL ,
 [LOPHOC_MaCB]			nvarchar(50) NULL ,
 [LOPHOC_PhongHoc]			nvarchar(50) NULL ,
 [LOPHOC_KhoaHoc]				nvarchar(50) NULL ,
 [LOPHOC_NamHoc]				nvarchar(50) NULL ,
 [LOPHOC_KyHoc]				nvarchar(50) NULL ,
 [LOPHOC_Thu]				tinyint NULL ,
 [LOPHOC_Tiet]				nvarchar(50) NULL ,
 [LOPHOC_SoSinhVien]				nvarchar(50) NULL ,
 [LOPHOC_NgayBatDau]				datetime NULL ,
 [LOPHOC_NgayKetThuc]				datetime NULL ,
 [LOPHOC_NgonNgu]				nvarchar(50) NULL ,
 [LOPHOC_NguoiTao]			nvarchar(50) NULL ,
 [LOPHOC_NgayTao]   datetime NULL ,
 [LOPHOC_TrangThai] varchar(1) NULL 
);
GO
-------------------[dbo].[LOPHOC_Insert] 6/27/2020----------------
create or alter proc [dbo].[LOPHOC_Insert]
 @LOPHOC_MaLopHoc				nvarchar(50) NULL ,
 @LOPHOC_MaMon			nvarchar(50) NULL ,
 @LOPHOC_MaCB			nvarchar(50) NULL ,
 @LOPHOC_PhongHoc			nvarchar(50) NULL ,
 @LOPHOC_KhoaHoc				nvarchar(50) NULL ,
 @LOPHOC_NamHoc				nvarchar(50) NULL ,
 @LOPHOC_KyHoc				nvarchar(50) NULL ,
 @LOPHOC_Thu				tinyint NULL ,
 @LOPHOC_Tiet				nvarchar(50) NULL ,
 @LOPHOC_SoSinhVien				nvarchar(50) NULL ,
 @LOPHOC_NgayBatDau				datetime NULL ,
 @LOPHOC_NgayKetThuc				datetime NULL ,
 @LOPHOC_NgonNgu				nvarchar(50) NULL ,
 @LOPHOC_NguoiTao			nvarchar(50) NULL ,
 @LOPHOC_NgayTao   datetime NULL ,
 @LOPHOC_TrangThai varchar(1) NULL 
as

if(exists(select * from LOPHOC where LOPHOC_MaMon= @LOPHOC_MaMon))
begin
	select '1' as Result, N'Môn học đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[LOPHOC]
    ( 
 [LOPHOC_MaLopHoc]				 ,
 [LOPHOC_MaMon]			 ,
 [LOPHOC_MaCB]			 ,
 [LOPHOC_PhongHoc]			 ,
 [LOPHOC_KhoaHoc]				 ,
 [LOPHOC_NamHoc]				 ,
 [LOPHOC_KyHoc]				 ,
 [LOPHOC_Thu]				,
 [LOPHOC_Tiet]				 ,
 [LOPHOC_SoSinhVien]				 ,
 [LOPHOC_NgayBatDau]				,
 [LOPHOC_NgayKetThuc]				,
 [LOPHOC_NgonNgu]				 ,
 [LOPHOC_NguoiTao]			 ,
 [LOPHOC_NgayTao]   ,
 [LOPHOC_TrangThai] )
	VALUES(   
 @LOPHOC_MaLopHoc				 ,
 @LOPHOC_MaMon			 ,
 @LOPHOC_MaCB			 ,
 @LOPHOC_PhongHoc			 ,
 @LOPHOC_KhoaHoc				 ,
 @LOPHOC_NamHoc				 ,
 @LOPHOC_KyHoc				 ,
 @LOPHOC_Thu				,
 @LOPHOC_Tiet				 ,
 @LOPHOC_SoSinhVien				 ,
 @LOPHOC_NgayBatDau				,
 @LOPHOC_NgayKetThuc				,
 @LOPHOC_NgonNgu				 ,
 @LOPHOC_NguoiTao			 ,
 @LOPHOC_NgayTao   ,
 @LOPHOC_TrangThai )
	declare @LOPHOC_Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LOPHOC_Ma as LOPHOC_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[LOPHOC_Update] 6/27/2020----------------

create or alter proc [dbo].[LOPHOC_Update]
  @LOPHOC_Ma int = NULL,
 @LOPHOC_MaLopHoc				nvarchar(50) NULL ,
 @LOPHOC_MaMon			nvarchar(50) NULL ,
 @LOPHOC_MaCB			nvarchar(50) NULL ,
 @LOPHOC_PhongHoc			nvarchar(50) NULL ,
 @LOPHOC_KhoaHoc				nvarchar(50) NULL ,
 @LOPHOC_NamHoc				nvarchar(50) NULL ,
 @LOPHOC_KyHoc				nvarchar(50) NULL ,
 @LOPHOC_Thu				tinyint NULL ,
 @LOPHOC_Tiet				nvarchar(50) NULL ,
 @LOPHOC_SoSinhVien				nvarchar(50) NULL ,
 @LOPHOC_NgayBatDau				datetime NULL ,
 @LOPHOC_NgayKetThuc				datetime NULL ,
 @LOPHOC_NgonNgu				nvarchar(50) NULL ,
 @LOPHOC_NguoiTao			nvarchar(50) NULL ,
 @LOPHOC_NgayTao   datetime NULL ,
 @LOPHOC_TrangThai varchar(1) NULL 

as

if(not exists(select * from LOPHOC where LOPHOC_Ma = @LOPHOC_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[LOPHOC]
	   SET
 [LOPHOC_MaLopHoc]	= @LOPHOC_MaLopHoc			 ,
 [LOPHOC_MaMon]		= @LOPHOC_MaMon	 ,
 [LOPHOC_MaCB]		= @LOPHOC_MaCB	 ,
 [LOPHOC_PhongHoc]	= @LOPHOC_PhongHoc	 ,
 [LOPHOC_KhoaHoc]	= @LOPHOC_KhoaHoc	 ,
 [LOPHOC_NamHoc]	= @LOPHOC_NamHoc		 ,
 [LOPHOC_KyHoc]		= @LOPHOC_KyHoc		 ,
 [LOPHOC_Thu]		= @LOPHOC_Thu		,
 [LOPHOC_Tiet]		= @LOPHOC_Tiet	 ,
 [LOPHOC_SoSinhVien]= @LOPHOC_SoSinhVien				 ,
 [LOPHOC_NgayBatDau]= @LOPHOC_NgayBatDau				,
 [LOPHOC_NgayKetThuc]=@LOPHOC_NgayKetThuc				,
 [LOPHOC_NgonNgu]	= @LOPHOC_NgonNgu			 ,
 [LOPHOC_NguoiTao]	= @LOPHOC_NguoiTao		 ,
 [LOPHOC_NgayTao]   = @LOPHOC_NgayTao,
 [LOPHOC_TrangThai]	= @LOPHOC_TrangThai
	WHERE LOPHOC_Ma = @LOPHOC_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LOPHOC_Ma as LOPHOC_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[LOPHOC_ById] 6/27/2020----------------

create or alter proc [dbo].[LOPHOC_ById]
    @Ma int = NULL
as
begin
select *
from LOPHOC
where LOPHOC_Ma = @Ma and LOPHOC_TrangThai = 'N'
end
go
-------------------[dbo].[LOPHOC_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[LOPHOC_SearchAll]
as
begin
select *
from LOPHOC
where LOPHOC_TrangThai = 'N'
end
go
-------------------[dbo].[LOPHOC_Search] 6/27/2020----------------
create or alter proc [dbo].[LOPHOC_Search] 
 @LOPHOC_Ma int = NULL,
 @LOPHOC_MaLopHoc				nvarchar(50) NULL ,
 @LOPHOC_MaMon			nvarchar(50) NULL ,
 @LOPHOC_MaCB			nvarchar(50) NULL ,
 @LOPHOC_PhongHoc			nvarchar(50) NULL ,
 @LOPHOC_KhoaHoc				nvarchar(50) NULL ,
 @LOPHOC_NamHoc				nvarchar(50) NULL ,
 @LOPHOC_KyHoc				nvarchar(50) NULL ,
 @LOPHOC_Thu				tinyint NULL ,
 @LOPHOC_Tiet				nvarchar(50) NULL ,
 @LOPHOC_SoSinhVien				nvarchar(50) NULL ,
 @LOPHOC_NgayBatDau				datetime NULL ,
 @LOPHOC_NgayKetThuc				datetime NULL ,
 @LOPHOC_NgonNgu				nvarchar(50) NULL ,
 @LOPHOC_NguoiTao			nvarchar(50) NULL ,
 @LOPHOC_NgayTao   datetime NULL ,
 @LOPHOC_TrangThai varchar(1) NULL 

as
begin
	select * from LOPHOC
	where (@LOPHOC_Ma is null or LOPHOC_Ma = @LOPHOC_Ma)
	  and (@LOPHOC_MaLopHoc is null or [LOPHOC_MaLopHoc]	= @LOPHOC_MaLopHoc			 )
  and (@LOPHOC_MaMon is null or [LOPHOC_MaMon]		= @LOPHOC_MaMon	 )
  and (@LOPHOC_MaCB is null or [LOPHOC_MaCB]		= @LOPHOC_MaCB	 )
  and (@LOPHOC_PhongHoc is null or [LOPHOC_PhongHoc]	= @LOPHOC_PhongHoc	 )
  and (@LOPHOC_KhoaHoc is null or [LOPHOC_KhoaHoc]	= @LOPHOC_KhoaHoc	 )
  and (@LOPHOC_NamHoc is null or [LOPHOC_NamHoc]	= @LOPHOC_NamHoc		 )
  and (@LOPHOC_KyHoc is null or [LOPHOC_KyHoc]		= @LOPHOC_KyHoc		 )
  and (@LOPHOC_Thu is null or [LOPHOC_Thu]		= @LOPHOC_Thu		)
  and (@LOPHOC_Tiet is null or [LOPHOC_Tiet]		= @LOPHOC_Tiet	 )
  and (@LOPHOC_SoSinhVien is null or [LOPHOC_SoSinhVien]= @LOPHOC_SoSinhVien				 )
  and (@LOPHOC_NgayBatDau is null or [LOPHOC_NgayBatDau]= @LOPHOC_NgayBatDau				)
  and (@LOPHOC_NgayKetThuc is null or [LOPHOC_NgayKetThuc]=@LOPHOC_NgayKetThuc				)
  and (@LOPHOC_NgonNgu is null or [LOPHOC_NgonNgu]	= @LOPHOC_NgonNgu			 )
 and (@LOPHOC_NguoiTao is null or [LOPHOC_NguoiTao]   = @LOPHOC_NguoiTao)
 and (@LOPHOC_NgayTao is null or [LOPHOC_NgayTao]   = @LOPHOC_NgayTao)
 and (@LOPHOC_TrangThai is null or [LOPHOC_TrangThai] =@LOPHOC_TrangThai)
end
go
-------------------[dbo].[LOPHOC_Delete] 6/27/2020----------------
create or alter proc [dbo].[LOPHOC_Delete] @LOPHOC_Ma int = NULL
as
begin transaction
begin try
	update LOPHOC set LOPHOC_TrangThai = 'X' where LOPHOC_Ma = @LOPHOC_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LOPHOC_Ma as LOPHOC_Ma
end try
begin catch

rollback transaction

end catch
go
