﻿CREATE TABLE [PHUCKHAO]
(
 [PHUCKHAO_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [PHUCKHAO_MaPhucKhao]				nvarchar(50) NULL ,
 [PHUCKHAO_MSSV]				nvarchar(50) NULL ,
 [PHUCKHAO_HoTen]			nvarchar(50) NULL ,
 [PHUCKHAO_Khoa]			nvarchar(50) NULL ,
 [PHUCKHAO_MaLop]			nvarchar(50) NULL ,
 [PHUCKHAO_CotDiemPK]				nvarchar(50) NULL ,
 [PHUCKHAO_ThoiGianPK]				float,
 [PHUCKHAO_DiemCu]				float,
 [PHUCKHAO_DiemMoi]				float,
 [PHUCKHAO_NguoiTao]  nvarchar(50) NULL ,
 [PHUCKHAO_NgayTao]   datetime NULL ,
 [PHUCKHAO_TrangThai] varchar(1) NULL ,
);
GO
-------------------[dbo].[PHUCKHAO_Insert] 6/27/2020----------------
create or alter proc [dbo].[PHUCKHAO_Insert]
@PHUCKHAO_MaPhucKhao				nvarchar(50) NULL ,
@PHUCKHAO_MSSV				nvarchar(50) NULL ,
@PHUCKHAO_HoTen			nvarchar(50) NULL ,
@PHUCKHAO_Khoa			nvarchar(50) NULL ,
@PHUCKHAO_MaLop			nvarchar(50) NULL ,
@PHUCKHAO_CotDiemPK				nvarchar(50) NULL ,
@PHUCKHAO_ThoiGianPK				float ,
@PHUCKHAO_DiemCu				float,
@PHUCKHAO_DiemMoi				float,
@PHUCKHAO_NguoiTao  nvarchar(50) NULL ,
@PHUCKHAO_NgayTao   datetime NULL ,
@PHUCKHAO_TrangThai varchar(1) NULL
as

if(exists(select * from PHUCKHAO where PHUCKHAO_MaPhucKhao= @PHUCKHAO_MaPhucKhao))
begin
	select '1' as Result, N'Phiếu phúc khảo đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[PHUCKHAO]
    ( 
 [PHUCKHAO_MaPhucKhao]				,
 [PHUCKHAO_MSSV]				,
 [PHUCKHAO_HoTen]			,
 [PHUCKHAO_Khoa]			,
 [PHUCKHAO_MaLop]			,
 [PHUCKHAO_CotDiemPK]				,
 [PHUCKHAO_ThoiGianPK]				,
 [PHUCKHAO_DiemCu]				,
 [PHUCKHAO_DiemMoi]				,
 [PHUCKHAO_NguoiTao]  ,
 [PHUCKHAO_NgayTao]   ,
 [PHUCKHAO_TrangThai]  )
	VALUES(   
@PHUCKHAO_MaPhucKhao				,
@PHUCKHAO_MSSV				,
@PHUCKHAO_HoTen			,
@PHUCKHAO_Khoa			,
@PHUCKHAO_MaLop			,
@PHUCKHAO_CotDiemPK				,
@PHUCKHAO_ThoiGianPK				,
@PHUCKHAO_DiemCu				,
@PHUCKHAO_DiemMoi				,
@PHUCKHAO_NguoiTao  ,
GetDate(),
'N'
)
	declare @Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @Ma as PHUCKHAO_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[PHUCKHAO_Update] 6/27/2020----------------

create or alter proc [dbo].[PHUCKHAO_Update]
    @PHUCKHAO_Ma int = NULL,
@PHUCKHAO_MaPhucKhao				nvarchar(50) NULL ,
@PHUCKHAO_MSSV				nvarchar(50) NULL ,
@PHUCKHAO_HoTen			nvarchar(50) NULL ,
@PHUCKHAO_Khoa			nvarchar(50) NULL ,
@PHUCKHAO_MaLop			nvarchar(50) NULL ,
@PHUCKHAO_CotDiemPK				nvarchar(50) NULL ,
@PHUCKHAO_ThoiGianPK				float,
@PHUCKHAO_DiemCu				float ,
@PHUCKHAO_DiemMoi				float,
@PHUCKHAO_NguoiTao  nvarchar(50) NULL ,
@PHUCKHAO_NgayTao   datetime NULL ,
@PHUCKHAO_TrangThai varchar(1) NULL

as

if(not exists(select * from PHUCKHAO where PHUCKHAO_Ma = @PHUCKHAO_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[PHUCKHAO]
	   SET 
	    [PHUCKHAO_MaPhucKhao]= @PHUCKHAO_Ma				,
 [PHUCKHAO_MSSV]	=@PHUCKHAO_MSSV			,
 [PHUCKHAO_HoTen]	=	@PHUCKHAO_HoTen	,
 [PHUCKHAO_Khoa]=@PHUCKHAO_Khoa	,
 [PHUCKHAO_MaLop]	=@PHUCKHAO_MaLop		,
 [PHUCKHAO_CotDiemPK]	=@PHUCKHAO_CotDiemPK			,
 [PHUCKHAO_ThoiGianPK]	=@PHUCKHAO_ThoiGianPK			,
 [PHUCKHAO_DiemCu]	=@PHUCKHAO_DiemCu			,
 [PHUCKHAO_DiemMoi]	=@PHUCKHAO_DiemMoi			,
 [PHUCKHAO_NguoiTao] =@PHUCKHAO_NguoiTao ,
 [PHUCKHAO_NgayTao]  =@PHUCKHAO_NgayTao ,
 [PHUCKHAO_TrangThai]  =@PHUCKHAO_TrangThai
	WHERE PHUCKHAO_Ma = @PHUCKHAO_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @PHUCKHAO_Ma as PHUCKHAO_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[PHUCKHAO_ById] 6/27/2020----------------

create or alter proc [dbo].[PHUCKHAO_ById]
    @PHUCKHAO_Ma int = NULL
as
begin
select *
from PHUCKHAO
where PHUCKHAO_Ma = @PHUCKHAO_Ma and PHUCKHAO_TrangThai = 'N'
end
go
-------------------[dbo].[PHUCKHAO_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[PHUCKHAO_SearchAll]
as
begin
select *
from PHUCKHAO
where PHUCKHAO_TrangThai = 'N'
end
go
-------------------[dbo].[PHUCKHAO_Search] 6/27/2020----------------
create or alter proc [dbo].[PHUCKHAO_Search] 
    @PHUCKHAO_Ma int = NULL,
@PHUCKHAO_MaPhucKhao				nvarchar(50) NULL ,
@PHUCKHAO_MSSV				nvarchar(50) NULL ,
@PHUCKHAO_HoTen			nvarchar(50) NULL ,
@PHUCKHAO_Khoa			nvarchar(50) NULL ,
@PHUCKHAO_MaLop			nvarchar(50) NULL ,
@PHUCKHAO_CotDiemPK				nvarchar(50) NULL ,
@PHUCKHAO_ThoiGianPK				float,
@PHUCKHAO_DiemCu				float ,
@PHUCKHAO_DiemMoi				float,
@PHUCKHAO_NguoiTao  nvarchar(50) NULL ,
@PHUCKHAO_NgayTao   datetime NULL ,
@PHUCKHAO_TrangThai varchar(1) NULL

as
begin
	select * from PHUCKHAO
	where (@PHUCKHAO_Ma is null or PHUCKHAO_Ma = @PHUCKHAO_Ma)
	and (@PHUCKHAO_MaPhucKhao is null or [PHUCKHAO_MaPhucKhao] = @PHUCKHAO_MaPhucKhao)
	and (@PHUCKHAO_MSSV is null or [PHUCKHAO_MSSV] = @PHUCKHAO_MSSV)
	and (@PHUCKHAO_HoTen is null or PHUCKHAO_HoTen= @PHUCKHAO_HoTen)
	and (@PHUCKHAO_Khoa is null or [PHUCKHAO_Khoa] = @PHUCKHAO_Khoa)
	and (@PHUCKHAO_MaLop is null or [PHUCKHAO_MaLop] = @PHUCKHAO_MaLop)
	and (@PHUCKHAO_CotDiemPK is null or [PHUCKHAO_CotDiemPK] = @PHUCKHAO_CotDiemPK)
	and (@PHUCKHAO_ThoiGianPK is null or [PHUCKHAO_ThoiGianPK] = @PHUCKHAO_ThoiGianPK)
	and (@PHUCKHAO_DiemCu is null or [PHUCKHAO_DiemCu] = @PHUCKHAO_DiemCu)
	and (@PHUCKHAO_DiemMoi is null or [PHUCKHAO_DiemMoi] = @PHUCKHAO_DiemMoi)
	and (@PHUCKHAO_NgayTao is null or [PHUCKHAO_NguoiTao] = @PHUCKHAO_NgayTao)
	and (@PHUCKHAO_NguoiTao is null or [PHUCKHAO_NgayTao] = @PHUCKHAO_NguoiTao)
	and (@PHUCKHAO_TrangThai is null or [PHUCKHAO_TrangThai] = @PHUCKHAO_TrangThai)
end
go
-------------------[dbo].[PHUCKHAO_Delete] 6/27/2020----------------
create or alter proc [dbo].[PHUCKHAO_Delete] @PHUCKHAO_Ma int = NULL
as
begin transaction
begin try
	update PHUCKHAO set PHUCKHAO_TrangThai = 'X' where PHUCKHAO_Ma = @PHUCKHAO_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @PHUCKHAO_Ma as PHUCKHAO_Ma
end try
begin catch

rollback transaction

end catch
go
