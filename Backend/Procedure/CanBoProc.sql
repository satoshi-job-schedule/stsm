﻿CREATE TABLE [CANBO]
(
 [CANBO_Ma]               int NOT NULL IDENTITY(1,1) ,
 [CANBO_MSCB]				nvarchar(50) NULL ,
 [CANBO_HoTen]			nvarchar(50) NULL ,
 [CANBO_GioiTinh]			nvarchar(50) NULL ,
 [CANBO_NgaySinh]			datetime NULL ,
 [CANBO_CMND]				nvarchar(50) NULL ,
 [CANBO_DiaChi]				nvarchar(50) NULL ,
 [CANBO_KhoaHoc]				nvarchar(50) NULL ,
 [CANBO_SoDienThoai]				nvarchar(50) NULL ,
 [CANBO_Khoa]				nvarchar(50) NULL ,
 [CANBO_Nganh]				nvarchar(50) NULL ,
 [CANBO_Lop]				nvarchar(50) NULL ,
 [CANBO_Email]				nvarchar(50) NULL ,
 [CANBO_TinhTrang]				nvarchar(50) NULL ,
 [CANBO_NguoiTao]  nvarchar(50) NULL ,
 [CANBO_NgayTao]   datetime NULL ,
 [CANBO_TrangThai] varchar(1) NULL ,
);
GO
-------------------[dbo].[CANBO_Insert] 6/27/2020----------------
create or alter proc [dbo].[CANBO_Insert]
@CANBO_MSCB varchar(50) NULL,
@CANBO_HoTen varchar(50) null,
@CANBO_GioiTinh nvarchar(50) NULL,
@CANBO_NgaySinh datetime NULL,
@CANBO_CMND nvarchar(50) null,
@CANBO_DiaChi varchar(50) NULL ,
@CANBO_KhoaHoc  nvarchar(50) NULL ,
@CANBO_SoDienThoai   nvarchar(50) NULL ,
@CANBO_Khoa nvarchar(50) null,
@CANBO_Nganh nvarchar(50) null,
@CANBO_Lop nvarchar(50) null,
@CANBO_Email nvarchar(50) null,
@CANBO_TinhTrang	nvarchar(50) NULL ,
@CANBO_NguoiTao  nvarchar(50) NULL ,
@CANBO_NgayTao   datetime NULL ,
@CANBO_TrangThai varchar(1) NULL 
as

if(exists(select * from CANBO where CANBO_MSCB= @CANBO_MSCB))
begin
	select '1' as Result, N'Sinh viên đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[CANBO]
    ( 
 [CANBO_MSCB]				 ,
 [CANBO_HoTen]			 ,
 [CANBO_GioiTinh]			 ,
 [CANBO_NgaySinh]			 ,
 [CANBO_CMND]				 ,
 [CANBO_DiaChi]				 ,
 [CANBO_KhoaHoc]				 ,
 [CANBO_SoDienThoai]				 ,
 [CANBO_Khoa]				 ,
 [CANBO_Nganh]				 ,
 [CANBO_Lop]				 ,
 [CANBO_Email]				 ,
 [CANBO_TinhTrang]				 ,
 [CANBO_NguoiTao]   ,
 [CANBO_NgayTao]    ,
 [CANBO_TrangThai]  )
	VALUES(   
 @CANBO_MSCB				 ,
 @CANBO_HoTen			 ,
 @CANBO_GioiTinh			 ,
 @CANBO_NgaySinh			 ,
 @CANBO_CMND				 ,
 @CANBO_DiaChi				 ,
 @CANBO_KhoaHoc				 ,
 @CANBO_SoDienThoai				 ,
 @CANBO_Khoa				 ,
 @CANBO_Nganh				 ,
 @CANBO_Lop				 ,
 @CANBO_Email				 ,
 'N'				 ,
 @CANBO_NguoiTao   ,
 GETDATE()    ,
 @CANBO_TrangThai )
	declare @Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @Ma as CANBO_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[CANBO_Update] 6/27/2020----------------

create or alter proc [dbo].[CANBO_Update]
    @CANBO_Ma int = NULL,
@CANBO_MSCB varchar(50) NULL,
@CANBO_HoTen varchar(50) null,
@CANBO_GioiTinh nvarchar(50) NULL,
@CANBO_NgaySinh datetime NULL,
@CANBO_CMND nvarchar(50) null,
@CANBO_DiaChi varchar(50) NULL ,
@CANBO_KhoaHoc  nvarchar(50) NULL ,
@CANBO_SoDienThoai   nvarchar(50) NULL ,
@CANBO_Khoa nvarchar(50) null,
@CANBO_Nganh nvarchar(50) null,
@CANBO_Lop nvarchar(50) null,
@CANBO_Email nvarchar(50) null,
@CANBO_TinhTrang	nvarchar(50) NULL ,
@CANBO_NguoiTao  nvarchar(50) NULL ,
@CANBO_NgayTao   datetime NULL ,
@CANBO_TrangThai varchar(1) NULL 

as

if(not exists(select * from CANBO where CANBO_Ma = @CANBO_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[CANBO]
	   SET
	   [CANBO_MSCB] = @CANBO_MSCB				,
[CANBO_HoTen] = @CANBO_HoTen			,
[CANBO_GioiTinh] = @CANBO_GioiTinh			,
[CANBO_NgaySinh]	= @CANBO_NgaySinh		,
[CANBO_CMND]	 = @CANBO_CMND			,
[CANBO_DiaChi] = @CANBO_DiaChi				,
[CANBO_KhoaHoc] = @CANBO_KhoaHoc				,
[CANBO_SoDienThoai] = @CANBO_SoDienThoai				,
[CANBO_Khoa] = @CANBO_Khoa				,
[CANBO_Nganh] = @CANBO_Nganh				,
[CANBO_Lop] = @CANBO_Lop				,
[CANBO_Email] = @CANBO_Email				,
[CANBO_TinhTrang] =@CANBO_TinhTrang				,
[CANBO_NguoiTao] = @CANBO_NgayTao,
[CANBO_NgayTao] = @CANBO_NguoiTao,
[CANBO_TrangThai] = @CANBO_TrangThai
	WHERE CANBO_Ma = @CANBO_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @CANBO_Ma as CANBO_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[CANBO_ById] 6/27/2020----------------

create or alter proc [dbo].[CANBO_ById]
    @Ma int = NULL
as
begin
select *
from CANBO
where CANBO_Ma = @Ma and CANBO_TrangThai = 'N'
end
go
-------------------[dbo].[CANBO_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[CANBO_SearchAll]
as
begin
select *
from CANBO
where CANBO_TrangThai = 'N'
end
go
-------------------[dbo].[CANBO_Search] 6/27/2020----------------
create or alter proc [dbo].[CANBO_Search] 
    @CANBO_Ma int = NULL,
@CANBO_MSCB varchar(50) NULL,
@CANBO_HoTen varchar(50) null,
@CANBO_GioiTinh nvarchar(50) NULL,
@CANBO_NgaySinh datetime NULL,
@CANBO_CMND nvarchar(50) null,
@CANBO_DiaChi varchar(50) NULL ,
@CANBO_KhoaHoc  nvarchar(50) NULL ,
@CANBO_SoDienThoai   nvarchar(50) NULL ,
@CANBO_Khoa nvarchar(50) null,
@CANBO_Nganh nvarchar(50) null,
@CANBO_Lop nvarchar(50) null,
@CANBO_Email nvarchar(50) null,
@CANBO_TinhTrang	nvarchar(50) NULL ,
@CANBO_NguoiTao  nvarchar(50) NULL ,
@CANBO_NgayTao   datetime NULL ,
@CANBO_TrangThai varchar(1) NULL 

as
begin
	select * from CANBO
	where (@CANBO_Ma is null or CANBO_Ma = @CANBO_Ma)
	and (@CANBO_MSCB is null or [CANBO_MSCB] = @CANBO_MSCB)
	and (@CANBO_HoTen is null or CANBO_HoTen= @CANBO_HoTen)
	and (@CANBO_GioiTinh is null or [CANBO_GioiTinh] = @CANBO_GioiTinh)
	and (@CANBO_NgaySinh = null or [CANBO_NgaySinh]	= @CANBO_NgaySinh)
	and (@CANBO_CMND is null or [CANBO_CMND]	 = @CANBO_CMND)
	and (@CANBO_Diachi is null or [CANBO_DiaChi] = @CANBO_DiaChi)
	and (@CANBO_KhoaHoc is null or [CANBO_KhoaHoc] = @CANBO_KhoaHoc)
	and (@CANBO_SoDienThoai is null or [CANBO_SoDienThoai] = @CANBO_SoDienThoai)
	and (@CANBO_Khoa is null or [CANBO_Khoa] = @CANBO_Khoa)
	and (@CANBO_Nganh is null or [CANBO_Nganh] = @CANBO_Nganh)
	and (@CANBO_Lop is null or [CANBO_Lop] = @CANBO_Lop)
	and (@CANBO_Email is null or [CANBO_Email] = @CANBO_Email)
	and (@CANBO_TinhTrang is null or [CANBO_TinhTrang] =@CANBO_TinhTrang)
	and (@CANBO_NgayTao is null or [CANBO_NguoiTao] = @CANBO_NgayTao)
	and (@CANBO_NguoiTao is null or [CANBO_NgayTao] = @CANBO_NguoiTao)
	and (@CANBO_TrangThai is null or [CANBO_TrangThai] = @CANBO_TrangThai)
end
go
-------------------[dbo].[CANBO_Delete] 6/27/2020----------------
create or alter proc [dbo].[CANBO_Delete] @CANBO_Ma int = NULL
as
begin transaction
begin try
	update CANBO set CANBO_TrangThai = 'X' where CANBO_Ma = @CANBO_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @CANBO_Ma as CANBO_Ma
end try
begin catch

rollback transaction

end catch
go
