CREATE TABLE [SINHVIEN]
(
 [SINHVIEN_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [SINHVIEN_MSSV]				nvarchar(50) NULL ,
 [SINHVIEN_HoTen]			nvarchar(50) NULL ,
 [SINHVIEN_GioiTinh]			nvarchar(50) NULL ,
 [SINHVIEN_NgaySinh]			datetime NULL ,
 [SINHVIEN_CMND]				nvarchar(50) NULL ,
 [SINHVIEN_DiaChi]				nvarchar(50) NULL ,
 [SINHVIEN_KhoaHoc]				nvarchar(50) NULL ,
 [SINHVIEN_SoDienThoai]				nvarchar(50) NULL ,
 [SINHVIEN_Khoa]				nvarchar(50) NULL ,
 [SINHVIEN_Nganh]				nvarchar(50) NULL ,
 [SINHVIEN_Lop]				nvarchar(50) NULL ,
 [SINHVIEN_Email]				nvarchar(50) NULL ,
 [SINHVIEN_TinhTrang]				nvarchar(50) NULL ,
 [SINHVIEN_NguoiTao]  nvarchar(50) NULL ,
 [SINHVIEN_NgayTao]   datetime NULL ,
 [SINHVIEN_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [CANBO]
(
 [CANBO_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [CANBO_MSCB]				nvarchar(50) NULL ,
 [CANBO_HoTen]			nvarchar(50) NULL ,
 [CANBO_GioiTinh]			nvarchar(50) NULL ,
 [CANBO_NgaySinh]			datetime NULL ,
 [CANBO_CMND]				nvarchar(50) NULL ,
 [CANBO_DiaChi]				nvarchar(50) NULL ,
 [CANBO_KhoaHoc]				nvarchar(50) NULL ,
 [CANBO_SoDienThoai]				nvarchar(50) NULL ,
 [CANBO_Khoa]				nvarchar(50) NULL ,
 [CANBO_Nganh]				nvarchar(50) NULL ,
 [CANBO_Lop]				nvarchar(50) NULL ,
 [CANBO_Email]				nvarchar(50) NULL ,
 [CANBO_TinhTrang]				nvarchar(50) NULL ,
 [CANBO_NguoiTao]  nvarchar(50) NULL ,
 [CANBO_NgayTao]   datetime NULL ,
 [CANBO_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [MONHOC]
(
 [MONHOC_Ma]               int NOT NULL IDENTITY(1,1) ,
 [MONHOC_MaMon]				nvarchar(50) NULL ,
 [MONHOC_TenMon]			nvarchar(50) NULL ,
 [MONHOC_Khoa]			nvarchar(50) NULL ,
 [MONHOC_SoTC]			nvarchar(50) NULL ,
 [MONHOC_CMND]				nvarchar(50) NULL ,
 [MONHOC_TCLyThuyet]				tinyint NULL ,
 [MONHOC_TCThucHanh]				tinyint NULL ,
 [MONHOC_HeSoQT]				float NULL ,
 [MONHOC_HeSoGK]				float NULL ,
 [MONHOC_HeSoTH]				float NULL ,
 [MONHOC_HeSoCK]				float NULL ,
 [MONHOC_NguoiTao]  nvarchar(50) NULL ,
 [MONHOC_NgayTao]   datetime NULL ,
 [MONHOC_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [LOPHOC]
(
 [LOPHOC_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [LOPHOC_MaLopHoc]				nvarchar(50) NULL ,
 [LOPHOC_MaMon]			nvarchar(50) NULL ,
 [LOPHOC_MaCB]			nvarchar(50) NULL ,
 [LOPHOC_PhongHoc]			nvarchar(50) NULL ,
 [LOPHOC_KhoaHoc]				nvarchar(50) NULL ,
 [LOPHOC_NamHoc]				nvarchar(50) NULL ,
 [LOPHOC_KyHoc]				nvarchar(50) NULL ,
 [LOPHOC_Thu]				tinyint NULL ,
 [LOPHOC_Tiet]				nvarchar(50) NULL ,
 [LOPHOC_SoSinhVien]				nvarchar(50) NULL ,
 [LOPHOC_NgayBatDau]				datetime NULL ,
 [LOPHOC_NgayKetThuc]				datetime NULL ,
 [LOPHOC_NgonNgu]				nvarchar(50) NULL ,
 [LOPHOC_NguoiTao]			nvarchar(50) NULL ,
 [LOPHOC_NgayTao]   datetime NULL ,
 [LOPHOC_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [DIEM]
(
 [DIEM_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [DIEM_MaSV]				nvarchar(50) NULL ,
 [DIEM_MaLopHoc]			nvarchar(50) NULL ,
 [DIEM_DiemQT]			nvarchar(50) NULL ,
 [DIEM_DiemGK]			nvarchar(50) NULL ,
 [DIEM_DiemTH]				nvarchar(50) NULL ,
 [DIEM_DiemCK]				nvarchar(50) NULL ,
 [DIEM_DiemTB]				nvarchar(50) NULL ,
 [DIEM_TinhTrang]				nvarchar(50) NULL ,
 [DIEM_NguoiTao]  nvarchar(50) NULL ,
 [DIEM_NgayTao]   datetime NULL ,
 [DIEM_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [LICHTHI]
(
 [LICHTHI_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [LICHTHI_MaLichThi]				nvarchar(50) NULL ,
 [LICHTHI_MaLopHoc]			nvarchar(50) NULL ,
 [LICHTHI_MaKyThi]			nvarchar(50) NULL ,
 [LICHTHI_CaThi]			tinyint NULL ,
 [LICHTHI_ThuThi]			tinyint NULL ,
 [LICHTHI_NgayThi]				datetime NULL ,
 [LICHTHI_PhongThi]				nvarchar(50) NULL ,
 [LICHTHI_CanBo1]				nvarchar(50) NULL ,
 [LICHTHI_CanBo2]				nvarchar(50) NULL ,
 [LICHTHI_NguoiTao]  nvarchar(50) NULL ,
 [LICHTHI_NgayTao]   datetime NULL ,
 [LICHTHI_TrangThai] varchar(1) NULL ,
);
GO

CREATE TABLE [PHUCKHAO]
(
 [PHUCKHAO_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [PHUCKHAO_MaPhucKhao]				nvarchar(50) NULL ,
 [PHUCKHAO_MSSV]				nvarchar(50) NULL ,
 [PHUCKHAO_HoTen]			nvarchar(50) NULL ,
 [PHUCKHAO_Khoa]			nvarchar(50) NULL ,
 [PHUCKHAO_MaLop]			nvarchar(50) NULL ,
 [PHUCKHAO_CotDiemPK]				nvarchar(50) NULL ,
 [PHUCKHAO_ThoiGianPK]				float ,
 [PHUCKHAO_DiemCu]				float,
 [PHUCKHAO_DiemMoi]			float ,
 [PHUCKHAO_NguoiTao]  nvarchar(50) NULL ,
 [PHUCKHAO_NgayTao]   datetime NULL ,
 [PHUCKHAO_TrangThai] varchar(1) NULL ,
);
GO
CREATE TABLE [USER]
(
 [USER_Ma]               int PRIMARY KEY NOT NULL IDENTITY(1,1) ,
 [USER_Username]				nvarchar(50) NULL ,
 [USER_Password]				nvarchar(50) NULL ,
 )