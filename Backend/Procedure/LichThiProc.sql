﻿CREATE TABLE [LICHTHI]
(
 [LICHTHI_Ma]               int NOT NULL IDENTITY(1,1) ,
 [LICHTHI_MaLichThi]				nvarchar(50) NULL ,
 [LICHTHI_MaLopHoc]			nvarchar(50) NULL ,
  [LICHTHI_MaKyThi]			nvarchar(50) NULL ,
 [LICHTHI_CaThi]			tinyint NULL ,
 [LICHTHI_ThuThi]			tinyint NULL ,
 [LICHTHI_NgayThi]				datetime NULL ,
 [LICHTHI_PhongThi]				nvarchar(50) NULL ,
 [LICHTHI_CanBo1]				nvarchar(50) NULL ,
 [LICHTHI_CanBo2]				nvarchar(50) NULL ,
 [LICHTHI_NguoiTao]  nvarchar(50) NULL ,
 [LICHTHI_NgayTao]   datetime NULL ,
 [LICHTHI_TrangThai] varchar(1) NULL 
);
GO

-------------------[dbo].[LICHTHI_Insert] 6/27/2020----------------
create or alter proc [dbo].[LICHTHI_Insert]
 @LICHTHI_MaLichThi				nvarchar(50) NULL ,
 @LICHTHI_MaLopHoc			nvarchar(50) NULL ,
 @LICHTHI_MaKyThi			nvarchar(50) NULL ,
 @LICHTHI_CaThi			tinyint NULL ,
 @LICHTHI_ThuThi			tinyint NULL ,
 @LICHTHI_NgayThi				datetime NULL ,
 @LICHTHI_PhongThi				nvarchar(50) NULL ,
 @LICHTHI_CanBo1				nvarchar(50) NULL ,
 @LICHTHI_CanBo2				nvarchar(50) NULL ,
 @LICHTHI_NguoiTao			nvarchar(50) NULL ,
 @LICHTHI_NgayTao   datetime NULL ,
 @LICHTHI_TrangThai varchar(1) NULL 
as

if(exists(select * from LICHTHI where LICHTHI_MaLichThi= @LICHTHI_MaLichThi))
begin
	select '1' as Result, N'Lịch thi đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[LICHTHI]
    ( 
 [LICHTHI_MaLichThi]				,
 [LICHTHI_MaLopHoc]			,
 [LICHTHI_MaKyThi],
 [LICHTHI_CaThi]			,
 [LICHTHI_ThuThi]			,
 [LICHTHI_NgayThi]				,
 [LICHTHI_PhongThi]				,
 [LICHTHI_CanBo1]				,
 [LICHTHI_CanBo2]				,
 [LICHTHI_NguoiTao]			 ,
 [LICHTHI_NgayTao]   ,
 [LICHTHI_TrangThai] )
	VALUES(   
 @LICHTHI_MaLichThi				,
 @LICHTHI_MaLopHoc			,
 @LICHTHI_MaKyThi,
 @LICHTHI_CaThi			,
 @LICHTHI_ThuThi			,
 @LICHTHI_NgayThi				,
 @LICHTHI_PhongThi				,
 @LICHTHI_CanBo1				,
 @LICHTHI_CanBo2				,
 @LICHTHI_NguoiTao			 ,
 @LICHTHI_NgayTao   ,
 @LICHTHI_TrangThai )
	declare @LICHTHI_Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LICHTHI_Ma as LICHTHI_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[LICHTHI_Update] 6/27/2020----------------

create or alter proc [dbo].[LICHTHI_Update]
  @LICHTHI_Ma int = NULL,
 @LICHTHI_MaLichThi				nvarchar(50) NULL ,
 @LICHTHI_MaLopHoc			nvarchar(50) NULL ,
 @LICHTHI_MaKyThi			nvarchar(50) NULL ,
 @LICHTHI_CaThi			tinyint NULL ,
 @LICHTHI_ThuThi			tinyint NULL ,
 @LICHTHI_NgayThi				datetime NULL ,
 @LICHTHI_PhongThi				nvarchar(50) NULL ,
 @LICHTHI_CanBo1				nvarchar(50) NULL ,
 @LICHTHI_CanBo2				nvarchar(50) NULL ,
 @LICHTHI_NguoiTao			nvarchar(50) NULL ,
 @LICHTHI_NgayTao   datetime NULL ,
 @LICHTHI_TrangThai varchar(1) NULL 

as

if(not exists(select * from LICHTHI where LICHTHI_Ma = @LICHTHI_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[LICHTHI]
	   SET
 [LICHTHI_MaLichThi] = @LICHTHI_MaLichThi				,
 [LICHTHI_MaLopHoc]	=@LICHTHI_MaLopHoc		,
  [LICHTHI_MaKyThi]	=@LICHTHI_MaKyThi		,
 [LICHTHI_CaThi]	=@LICHTHI_CaThi		,
 [LICHTHI_ThuThi]	=@LICHTHI_ThuThi		,
 [LICHTHI_NgayThi]	=@LICHTHI_NgayThi			,
 [LICHTHI_PhongThi]	=@LICHTHI_PhongThi			,
 [LICHTHI_CanBo1]	=@LICHTHI_CanBo1			,
 [LICHTHI_CanBo2]	=@LICHTHI_CanBo2			,
 [LICHTHI_NguoiTao]	= @LICHTHI_NguoiTao		 ,
 [LICHTHI_NgayTao]   = @LICHTHI_NgayTao,
 [LICHTHI_TrangThai]	= @LICHTHI_TrangThai
	WHERE LICHTHI_Ma = @LICHTHI_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LICHTHI_Ma as LICHTHI_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[LICHTHI_ById] 6/27/2020----------------

create or alter proc [dbo].[LICHTHI_ById]
    @Ma int = NULL
as
begin
select *
from LICHTHI
where LICHTHI_Ma = @Ma and LICHTHI_TrangThai = 'N'
end
go
-------------------[dbo].[LICHTHI_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[LICHTHI_SearchAll]
as
begin
select *
from LICHTHI
where LICHTHI_TrangThai = 'N'
end
go
-------------------[dbo].[LICHTHI_Search] 6/27/2020----------------
create or alter proc [dbo].[LICHTHI_Search] 
 @LICHTHI_Ma int = NULL,
 @LICHTHI_MaLichThi				nvarchar(50) NULL ,
 @LICHTHI_MaLopHoc			nvarchar(50) NULL ,
 @LICHTHI_MaKyThi			nvarchar(50) NULL ,
 @LICHTHI_CaThi			tinyint NULL ,
 @LICHTHI_ThuThi			tinyint NULL ,
 @LICHTHI_NgayThi				datetime NULL ,
 @LICHTHI_PhongThi				nvarchar(50) NULL ,
 @LICHTHI_CanBo1				nvarchar(50) NULL ,
 @LICHTHI_CanBo2				nvarchar(50) NULL ,
 @LICHTHI_NguoiTao			nvarchar(50) NULL ,
 @LICHTHI_NgayTao   datetime NULL ,
 @LICHTHI_TrangThai varchar(1) NULL 

as
begin
	select * from LICHTHI
	where (@LICHTHI_Ma is null or LICHTHI_Ma = @LICHTHI_Ma)
	    and (@LICHTHI_MaLichThi is null or [LICHTHI_MaLichThi] = @LICHTHI_MaLichThi				)
  and (@LICHTHI_MaLopHoc is null or [LICHTHI_MaLopHoc]	=@LICHTHI_MaLopHoc		)
    and (@LICHTHI_MaLopHoc is null or [LICHTHI_MaLopHoc]	=@LICHTHI_MaLopHoc		)
  and (@LICHTHI_CaThi is null or [LICHTHI_CaThi]	=@LICHTHI_CaThi		)
  and (@LICHTHI_ThuThi is null or [LICHTHI_ThuThi]	=@LICHTHI_ThuThi		)
  and (@LICHTHI_NgayThi is null or [LICHTHI_NgayThi]	=@LICHTHI_NgayThi			)
  and (@LICHTHI_PhongThi is null or [LICHTHI_PhongThi]	=@LICHTHI_PhongThi			)
  and (@LICHTHI_CanBo1 is null or [LICHTHI_CanBo1]	=@LICHTHI_CanBo1			)
  and (@LICHTHI_CanBo2 is null or [LICHTHI_CanBo2]	=@LICHTHI_CanBo2			)
 and (@LICHTHI_NguoiTao is null or [LICHTHI_NguoiTao]   = @LICHTHI_NguoiTao)
 and (@LICHTHI_NgayTao is null or [LICHTHI_NgayTao]   = @LICHTHI_NgayTao)
 and (@LICHTHI_TrangThai is null or [LICHTHI_TrangThai] =@LICHTHI_TrangThai)
end
go
-------------------[dbo].[LICHTHI_Delete] 6/27/2020----------------
create or alter proc [dbo].[LICHTHI_Delete] @LICHTHI_Ma int = NULL
as
begin transaction
begin try
	update LICHTHI set LICHTHI_TrangThai = 'X' where LICHTHI_Ma = @LICHTHI_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @LICHTHI_Ma as LICHTHI_Ma
end try
begin catch

rollback transaction

end catch
go
