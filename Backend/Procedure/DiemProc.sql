﻿CREATE TABLE [DIEM]
(
 [DIEM_Ma]               int NOT NULL IDENTITY(1,1) ,
 [DIEM_MaSV]				nvarchar(50) NULL ,
 [DIEM_MaDIEM]			nvarchar(50) NULL ,
 [DIEM_DiemQT]			nvarchar(50) NULL ,
 [DIEM_DiemGK]			nvarchar(50) NULL ,
 [DIEM_DiemTH]				nvarchar(50) NULL ,
 [DIEM_DiemCK]				nvarchar(50) NULL ,
 [DIEM_DiemTB]				nvarchar(50) NULL ,
 [DIEM_TinhTrang]				nvarchar(50) NULL ,
 [DIEM_NguoiTao]  nvarchar(50) NULL ,
 [DIEM_NgayTao]   datetime NULL ,
 [DIEM_TrangThai] varchar(1) NULL ,
);
GO
-------------------[dbo].[DIEM_Insert] 6/27/2020----------------
create or alter proc [dbo].[DIEM_Insert]
@DIEM_MaSV				nvarchar(50) NULL ,
@DIEM_MaDIEM			nvarchar(50) NULL ,
@DIEM_DiemQT			nvarchar(50) NULL ,
@DIEM_DiemGK			nvarchar(50) NULL ,
@DIEM_DiemTH				nvarchar(50) NULL ,
@DIEM_DiemCK				nvarchar(50) NULL ,
@DIEM_DiemTB				nvarchar(50) NULL ,
@DIEM_TinhTrang				nvarchar(50) NULL ,
@DIEM_NguoiTao  nvarchar(50) NULL ,
@DIEM_NgayTao   datetime NULL ,
@DIEM_TrangThai varchar(1) NULL 
as

begin transaction
begin try

	INSERT INTO [dbo].[DIEM]
    ( 
 [DIEM_MaSV]				,
 [DIEM_MaDIEM]			,
 [DIEM_DiemQT]			,
 [DIEM_DiemGK]			,
 [DIEM_DiemTH]				,
 [DIEM_DiemCK]				,
 [DIEM_DiemTB]				,
 [DIEM_TinhTrang]				,
 [DIEM_NguoiTao]			 ,
 [DIEM_NgayTao]   ,
 [DIEM_TrangThai] )
	VALUES(   
@DIEM_MaSV				,
@DIEM_MaDIEM			,
@DIEM_DiemQT			,
@DIEM_DiemGK			,
@DIEM_DiemTH				,
@DIEM_DiemCK				,
@DIEM_DiemTB				,
@DIEM_TinhTrang				,
 @DIEM_NguoiTao			 ,
 @DIEM_NgayTao   ,
 @DIEM_TrangThai )
	declare @DIEM_Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @DIEM_Ma as DIEM_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[DIEM_Update] 6/27/2020----------------

create or alter proc [dbo].[DIEM_Update]
  @DIEM_Ma int = NULL,
@DIEM_MaSV				nvarchar(50) NULL ,
@DIEM_MaDIEM			nvarchar(50) NULL ,
@DIEM_DiemQT			nvarchar(50) NULL ,
@DIEM_DiemGK			nvarchar(50) NULL ,
@DIEM_DiemTH				nvarchar(50) NULL ,
@DIEM_DiemCK				nvarchar(50) NULL ,
@DIEM_DiemTB				nvarchar(50) NULL ,
@DIEM_TinhTrang				nvarchar(50) NULL ,
@DIEM_NguoiTao  nvarchar(50) NULL ,
@DIEM_NgayTao   datetime NULL ,
@DIEM_TrangThai varchar(1) NULL 

as

if(not exists(select * from DIEM where DIEM_Ma = @DIEM_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[DIEM]
	   SET
  [DIEM_MaSV]	= @DIEM_MaSV			,
 [DIEM_MaDIEM]	=@DIEM_MaDIEM		,
 [DIEM_DiemQT]	=@DIEM_DiemQT		,
 [DIEM_DiemGK]	=@DIEM_DiemGK		,
 [DIEM_DiemTH]	=@DIEM_DiemTH			,
 [DIEM_DiemCK]	=@DIEM_DiemCK			,
 [DIEM_DiemTB]	=@DIEM_DiemTB			,
 [DIEM_TinhTrang]=@DIEM_TinhTrang				,
 [DIEM_NguoiTao]	= @DIEM_NguoiTao		 ,
 [DIEM_NgayTao]   = @DIEM_NgayTao,
 [DIEM_TrangThai]	= @DIEM_TrangThai
	WHERE DIEM_Ma = @DIEM_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @DIEM_Ma as DIEM_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[DIEM_ById] 6/27/2020----------------

create or alter proc [dbo].[DIEM_ById]
    @Ma int = NULL
as
begin
select *
from DIEM
where DIEM_Ma = @Ma and DIEM_TrangThai = 'N'
end
go
-------------------[dbo].[DIEM_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[DIEM_SearchAll]
as
begin
select *
from DIEM
where DIEM_TrangThai = 'N'
end
go
-------------------[dbo].[DIEM_Search] 6/27/2020----------------
create or alter proc [dbo].[DIEM_Search] 
  @DIEM_Ma int = NULL,
@DIEM_MaSV				nvarchar(50) NULL ,
@DIEM_MaDIEM			nvarchar(50) NULL ,
@DIEM_DiemQT			nvarchar(50) NULL ,
@DIEM_DiemGK			nvarchar(50) NULL ,
@DIEM_DiemTH				nvarchar(50) NULL ,
@DIEM_DiemCK				nvarchar(50) NULL ,
@DIEM_DiemTB				nvarchar(50) NULL ,
@DIEM_TinhTrang				nvarchar(50) NULL ,
@DIEM_NguoiTao  nvarchar(50) NULL ,
@DIEM_NgayTao   datetime NULL ,
@DIEM_TrangThai varchar(1) NULL 

as
begin
	select * from DIEM
	where (@DIEM_Ma is null or DIEM_Ma = @DIEM_Ma)
	 and (@DIEM_MaSV is null or [DIEM_MaSV]	= @DIEM_MaSV			)
and (@DIEM_MaDIEM is null or [DIEM_MaDIEM]	=@DIEM_MaDIEM		)
and (@DIEM_DiemQT is null or [DIEM_DiemQT]	=@DIEM_DiemQT		)
and (@DIEM_DiemGK is null or [DIEM_DiemGK]	=@DIEM_DiemGK		)
and (@DIEM_DiemTH is null or [DIEM_DiemTH]	=@DIEM_DiemTH			)
and (@DIEM_DiemCK is null or [DIEM_DiemCK]	=@DIEM_DiemCK			)
and (@DIEM_DiemTB is null or [DIEM_DiemTB]	=@DIEM_DiemTB			)
and (@DIEM_TinhTrang is null or [DIEM_TinhTrang]=@DIEM_TinhTrang				)
 and (@DIEM_NguoiTao is null or [DIEM_NguoiTao]   = @DIEM_NguoiTao)
 and (@DIEM_NgayTao is null or [DIEM_NgayTao]   = @DIEM_NgayTao)
 and (@DIEM_TrangThai is null or [DIEM_TrangThai] =@DIEM_TrangThai)
end
go
-------------------[dbo].[DIEM_Delete] 6/27/2020----------------
create or alter proc [dbo].[DIEM_Delete] @DIEM_Ma int = NULL
as
begin transaction
begin try
	update DIEM set DIEM_TrangThai = 'X' where DIEM_Ma = @DIEM_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @DIEM_Ma as DIEM_Ma
end try
begin catch

rollback transaction

end catch
go
