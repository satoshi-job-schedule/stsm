﻿
CREATE TABLE [MONHOC]
(
 [MONHOC_Ma]               int NOT NULL IDENTITY(1,1) ,
 [MONHOC_MaMon]				nvarchar(50) NULL ,
 [MONHOC_TenMon]			nvarchar(50) NULL ,
 [MONHOC_Khoa]			nvarchar(50) NULL ,
 [MONHOC_SoTC]			nvarchar(50) NULL ,
 [MONHOC_CMND]				nvarchar(50) NULL ,
 [MONHOC_TCLyThuyet]				tinyint NULL ,
 [MONHOC_TCThucHanh]				tinyint NULL ,
 [MONHOC_HeSoQT]				float NULL ,
 [MONHOC_HeSoGK]				float NULL ,
 [MONHOC_HeSoTH]				float NULL ,
 [MONHOC_HeSoCK]				float NULL ,
 [MONHOC_NguoiTao]  nvarchar(50) NULL ,
 [MONHOC_NgayTao]   datetime NULL ,
 [MONHOC_TrangThai] varchar(1) NULL ,
);
GO
-------------------[dbo].[MONHOC_Insert] 6/27/2020----------------
create or alter proc [dbo].[MONHOC_Insert]
@MONHOC_MaMon				nvarchar(50) NULL ,
@MONHOC_TenMon			nvarchar(50) NULL ,
@MONHOC_Khoa			nvarchar(50) NULL ,
@MONHOC_SoTC			nvarchar(50) NULL ,
@MONHOC_CMND				nvarchar(50) NULL ,
@MONHOC_TCLyThuyet				tinyint NULL ,
@MONHOC_TCThucHanh				tinyint NULL ,
@MONHOC_HeSoQT				float NULL ,
@MONHOC_HeSoGK				float NULL ,
@MONHOC_HeSoTH				float NULL ,
@MONHOC_HeSoCK				float NULL ,
@MONHOC_NguoiTao  nvarchar(50) NULL ,
@MONHOC_NgayTao   datetime NULL ,
@MONHOC_TrangThai varchar(1) NULL 
as

if(exists(select * from MONHOC where MONHOC_MaMon= @MONHOC_MaMon))
begin
	select '1' as Result, N'Môn học đã tồn tại trong hệ thống' as ErrorDesc
	return
end
else

begin transaction
begin try

	INSERT INTO [dbo].[MONHOC]
    ( 
[MONHOC_MaMon]				 ,
 [MONHOC_TenMon]			 ,
 [MONHOC_Khoa]			 ,
 [MONHOC_SoTC]			 ,
 [MONHOC_CMND]				 ,
 [MONHOC_TCLyThuyet]				,
 [MONHOC_TCThucHanh]				,
 [MONHOC_HeSoQT]				,
 [MONHOC_HeSoGK]				,
 [MONHOC_HeSoTH]				,
 [MONHOC_HeSoCK]				,
 [MONHOC_NguoiTao]   ,
 [MONHOC_NgayTao]   ,
 [MONHOC_TrangThai] )
	VALUES(   
@MONHOC_MaMon				 ,
@MONHOC_TenMon			 ,
@MONHOC_Khoa			 ,
@MONHOC_SoTC			 ,
@MONHOC_CMND				 ,
@MONHOC_TCLyThuyet				,
@MONHOC_TCThucHanh				,
@MONHOC_HeSoQT				,
@MONHOC_HeSoGK				,
@MONHOC_HeSoTH				,
@MONHOC_HeSoCK				,
@MONHOC_NguoiTao   ,
@MONHOC_NgayTao   ,
@MONHOC_TrangThai )
	declare @MONHOC_Ma int = SCOPE_IDENTITY()
commit transaction
	select '0' as Result, N'' as ErrorDesc, @MONHOC_Ma as MONHOC_Ma
end try
begin catch

rollback transaction

end catch
go
-------------------[dbo].[MONHOC_Update] 6/27/2020----------------

create or alter proc [dbo].[MONHOC_Update]
  @MONHOC_Ma int = NULL,
@MONHOC_MaMon				nvarchar(50) NULL ,
@MONHOC_TenMon			nvarchar(50) NULL ,
@MONHOC_Khoa			nvarchar(50) NULL ,
@MONHOC_SoTC			nvarchar(50) NULL ,
@MONHOC_CMND				nvarchar(50) NULL ,
@MONHOC_TCLyThuyet				tinyint NULL ,
@MONHOC_TCThucHanh				tinyint NULL ,
@MONHOC_HeSoQT				float NULL ,
@MONHOC_HeSoGK				float NULL ,
@MONHOC_HeSoTH				float NULL ,
@MONHOC_HeSoCK				float NULL ,
@MONHOC_NguoiTao  nvarchar(50) NULL ,
@MONHOC_NgayTao   datetime NULL ,
@MONHOC_TrangThai varchar(1) NULL 

as

if(not exists(select * from MONHOC where MONHOC_Ma = @MONHOC_Ma))
begin
	select '1' as Result, N'Hoạt động không tồn tại trong hệ thống' as ErrorDesc
	RETURN
end
begin transaction
begin try

	UPDATE [dbo].[MONHOC]
	   SET
[MONHOC_MaMon]	= @MONHOC_MaMon			 ,
 [MONHOC_TenMon] = @MONHOC_TenMon			 ,
 [MONHOC_Khoa] = @MONHOC_Khoa			 ,
 [MONHOC_SoTC] = @MONHOC_SoTC			 ,
 [MONHOC_CMND]	= @MONHOC_CMND			 ,
 [MONHOC_TCLyThuyet] = @MONHOC_TCLyThuyet			,
 [MONHOC_TCThucHanh] = @MONHOC_TCThucHanh				,
 [MONHOC_HeSoQT]	= @MONHOC_HeSoQT			,
 [MONHOC_HeSoGK]	= @MONHOC_HeSoGK			,
 [MONHOC_HeSoTH]	= @MONHOC_HeSoTH			,
 [MONHOC_HeSoCK]	= @MONHOC_HeSoCK			,
 [MONHOC_NguoiTao]   = @MONHOC_NguoiTao,
 [MONHOC_NgayTao]   = @MONHOC_NgayTao,
 [MONHOC_TrangThai] =@MONHOC_TrangThai
	WHERE MONHOC_Ma = @MONHOC_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @MONHOC_Ma as MONHOC_Ma
end try
begin catch

rollback transaction

end catch
go

-------------------[dbo].[MONHOC_ById] 6/27/2020----------------

create or alter proc [dbo].[MONHOC_ById]
    @Ma int = NULL
as
begin
select *
from MONHOC
where MONHOC_Ma = @Ma and MONHOC_TrangThai = 'N'
end
go
-------------------[dbo].[MONHOC_SearchAll] 6/27/2020----------------
create or alter proc [dbo].[MONHOC_SearchAll]
as
begin
select *
from MONHOC
where MONHOC_TrangThai = 'N'
end
go
-------------------[dbo].[MONHOC_Search] 6/27/2020----------------
create or alter proc [dbo].[MONHOC_Search] 
  @MONHOC_Ma int = NULL,
@MONHOC_MaMon				nvarchar(50) NULL ,
@MONHOC_TenMon			nvarchar(50) NULL ,
@MONHOC_Khoa			nvarchar(50) NULL ,
@MONHOC_SoTC			nvarchar(50) NULL ,
@MONHOC_CMND				nvarchar(50) NULL ,
@MONHOC_TCLyThuyet				tinyint NULL ,
@MONHOC_TCThucHanh				tinyint NULL ,
@MONHOC_HeSoQT				float NULL ,
@MONHOC_HeSoGK				float NULL ,
@MONHOC_HeSoTH				float NULL ,
@MONHOC_HeSoCK				float NULL ,
@MONHOC_NguoiTao  nvarchar(50) NULL ,
@MONHOC_NgayTao   datetime NULL ,
@MONHOC_TrangThai varchar(1) NULL 

as
begin
	select * from MONHOC
	where (@MONHOC_Ma is null or MONHOC_Ma = @MONHOC_Ma)
	and (@MONHOC_MaMon is null or [MONHOC_MaMon]	= @MONHOC_MaMon			 )
 and (@MONHOC_TenMon is null or [MONHOC_TenMon] = @MONHOC_TenMon			 )
 and (@MONHOC_Khoa is null or [MONHOC_Khoa] = @MONHOC_Khoa			 )
 and (@MONHOC_SoTC is null or [MONHOC_SoTC] = @MONHOC_SoTC			 )
 and (@MONHOC_CMND is null or [MONHOC_CMND]	= @MONHOC_CMND			 )
 and (@MONHOC_TCLyThuyet is null or [MONHOC_TCLyThuyet] = @MONHOC_TCLyThuyet			)
 and (@MONHOC_TCThucHanh is null or [MONHOC_TCThucHanh] = @MONHOC_TCThucHanh				)
 and (@MONHOC_HeSoQT is null or [MONHOC_HeSoQT]	= @MONHOC_HeSoQT			)
 and (@MONHOC_HeSoGK is null or [MONHOC_HeSoGK]	= @MONHOC_HeSoGK			)
 and (@MONHOC_HeSoTH is null or [MONHOC_HeSoTH]	= @MONHOC_HeSoTH			)
 and (@MONHOC_HeSoCK is null or [MONHOC_HeSoCK]	= @MONHOC_HeSoCK			)
 and (@MONHOC_NguoiTao is null or [MONHOC_NguoiTao]   = @MONHOC_NguoiTao)
 and (@MONHOC_NgayTao is null or [MONHOC_NgayTao]   = @MONHOC_NgayTao)
 and (@MONHOC_TrangThai is null or [MONHOC_TrangThai] =@MONHOC_TrangThai)
end
go
-------------------[dbo].[MONHOC_Delete] 6/27/2020----------------
create or alter proc [dbo].[MONHOC_Delete] @MONHOC_Ma int = NULL
as
begin transaction
begin try
	update MONHOC set MONHOC_TrangThai = 'X' where MONHOC_Ma = @MONHOC_Ma
commit transaction
	select '0' as Result, N'' as ErrorDesc, @MONHOC_Ma as MONHOC_Ma
end try
begin catch

rollback transaction

end catch
go
